<?php

/**
 * @file
 * readingroom_image administration forms
 */

/**
 * Page call back for generating module settings form
 * @param $form
 * @param $form_state
 * @return mixed
 */
function _readingroom_image_settings_form($form, /** @noinspection PhpUnusedParameterInspection */   &$form_state) {
  $acc_lev = array();
  for ($i = 0; $i< variable_get(DRR_PREFIX . 'public_access_level', 2); $i++) {
    $acc_lev[] = $i;
  }
  $acc_lev[] = 'public';
  $form['wp_access_level'] = array(
    '#type' => 'select',
    '#title' => t('Default Waypoint Access Level'),
    '#required' => FALSE,
    '#description' => t('Default waypoint access level for new created waypoints'),
    '#options' => $acc_lev,
    '#default_value' => variable_get(DRR_PREFIX . 'i_def_wp', 1),
  );
  $form['image_access_level'] = array(
    '#type' => 'select',
    '#title' => t('Default Image Access Level'),
    '#required' => FALSE,
    '#description' => t('Default image access level for new imported images'),
    '#options' => $acc_lev,
    '#default_value' => variable_get(DRR_PREFIX . 'i_image_level', 1),
  );
  
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['cancel'] = array(
    '#type' => 'button',
    '#value' => t('Cancel'),
    '#attributes' => array('onClick' => 'location.replace("' . $_SERVER['HTTP_REFERER'] . '"); return false;'),
    '#weight' => 20,
  );
  
  return $form;
}

/**
 * submit function for _readingroom_image_settings_form
 * @param $form
 * @param $form_state
 */
function _readingroom_image_settings_form_submit(/** @noinspection PhpUnusedParameterInspection */ $form, &$form_state) {
  $wp = check_plain($form_state['values']['wp_access_level']);
  if ($wp != variable_get(DRR_PREFIX . 'i_def_wp', 1)) variable_set(DRR_PREFIX . 'i_def_wp', $wp);
  $im = check_plain($form_state['values']['image_access_level']);
  if ($im != variable_get(DRR_PREFIX . 'i_def_im', 1)) variable_set(DRR_PREFIX . 'i_def_im', $wp);
  $form_state['redirect'] = '';
}
