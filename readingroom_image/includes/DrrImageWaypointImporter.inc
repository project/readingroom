<?php
/**
 * @file
 * file for class DrrImageWaypointImporter
 */

/**
 * Class DrrImageWaypointImporter
 */
class DrrImageWaypointImporter extends DrrImageImporter {
  private $path;  // path to waypoint file
  private $update_images = FALSE;

  /**
   * constructor
   * @param $dir string
   * @param $file string
   */
  public function __construct($dir, $file) {
    $this->path = $dir . $file;
  }

  /**
   * start to import the waypoint file
   */
  public function start_import() {
    $fh = fopen($this->path, 'r');
    fgets($fh);  // skip first line
    $line = fgets($fh);
    $columns = new stdClass();
    while ($line[0]=='#') {
      $setting = json_decode(substr($line, 1));
      if (array_key_exists('columns', $setting)) {
        foreach ($setting->columns as $key => $value) {
          $columns->$key = $value;
        }
      }
      $line = fgets($fh);
    }
    if (!property_exists($columns, 'ID')) {
      self::watchdog('Invalid format. Missing "ID"-column in @filename', array('@filename' => $this->path), WATCHDOG_WARNING);
    }
    else { // analyse headers

      $separator = _readingroom_get_separator($line);
      // make sure to have (right) column numbers
      $headers = array_flip(explode($separator, str_replace(array("\r", "\n"), "", $line)));
      foreach ($columns as $key => $value) {
        if (array_key_exists('number', $value)) {
          $columns->$key = $value->number - 1;
          if ($columns->$key<0 || $columns->$key>= count($headers)) {
            self::watchdog('Column number out of range. Skipping column @column in @filename',
              array('@filename' => $this->path, '@column' => $key), WATCHDOG_WARNING);
            unset($columns->$key);
          }
        }
        else {
          if (!array_key_exists('header', $value)) {
            self::watchdog('Invalid header format. "header" or "number" parameter is required for column @column in @filename',
              array('@filename' => $this->path, '@column' => $key), WATCHDOG_WARNING);
            unset($columns->$key);
          }
          else {
            if (!array_key_exists($value->header, $headers)) {
              self::watchdog('Header @header not found. Skipping column in @filename',
                array('@filename' => $this->path, '@header' => $value->header), WATCHDOG_WARNING);
              unset($columns->$key);
            }
            else {
              $columns->$key = $headers[$value->header];
            }
          }
        }
      }

      if (!property_exists($columns, 'ID')) {
        self::watchdog('No valid ID column found. Skipping file @filename',
          array('@filename' => $this->path), WATCHDOG_WARNING);
        fclose($fh);
        return;
      }

      $info = entity_get_property_info(DRR_ENTITY_WAYPOINT);
      foreach ($columns as $key => $value) {
        if ($key=='ID') continue;
        if (!array_key_exists(DRR_PREFIX . $key, $info['bundles'][DRR_BUNDLE_WAYPOINT]['properties'])) {
          self::watchdog('No valid column property @property. Skipping column in @filename.',
            array('@property' => $key, '@filename' => $this->path), WATCHDOG_WARNING);
          unset($columns->$key);
        }
      }

      if (count((array)$columns)<2) {
        self::watchdog('To less valid columns. Skipping file @filename',
          array('@filename' => $this->path), WATCHDOG_WARNING);
        fclose($fh);
        return;
      }

      // import the data
      while (($line=fgetcsv($fh, 0, $separator, '"'))!==FALSE) {
        $id = $line[$columns->ID];
        $waypoint = $this->waypointLoadById($id);
        if (!$waypoint) {
          // if no waypoint found create a new one
          $waypoint = readingroom_entity_create(DRR_ENTITY_WAYPOINT, DRR_BUNDLE_WAYPOINT);
          if (is_int($id)) {  // if integer then using dgs number
            $waypoint->{DRR_PREFIX . 'folder_id'} = $id;
          }
          else {
            $waypoint->{DRR_PREFIX . 'uad'} = $id;
          }
        }
        else {
          $waypoint = entity_metadata_wrapper(DRR_ENTITY_WAYPOINT, $waypoint,  array('bundle' => DRR_BUNDLE_WAYPOINT));
          $this->update_images = TRUE;
        }
        foreach ($columns as $key => $value) {
          if ($key=='ID') continue;
          // TODO: skip non existing fields
          if ($waypoint->{DRR_PREFIX . $key}->type() == 'integer') {
            $waypoint->{DRR_PREFIX . $key} = (int)$line[$value];
          }
          else {
            $waypoint->{DRR_PREFIX . $key} = $line[$value];
          }
          if (in_array($key, ['folder_id', 'uad'])) {
            $this->add_waypoint_id($waypoint->id->value(), $line[$value]);
          }
        }
        $waypoint->save();
      }
    }
    fclose($fh);
  }

  private function add_waypoint_id($id, $add_id) {
    $m = new MongoClient();
    $db = $m->selectDB('drupal');
    $collection = new MongoCollection($db, 'fields_current.' . DRR_PREFIX . 'imagelists');

    $waypointQuery = array(DRR_PREFIX . 'waypoint_ref.target_id' => intval($id));
    $images = $collection->find($waypointQuery);
    global $base_root;
    $field = is_numeric($add_id)?'folder_id':'folder_uad';
    foreach ($images as $image) {
      $image_id = $image[DRR_PREFIX . 'image_ref']['target_id'];
      $image_entity = entity_load(DRR_ENTITY_IMAGE, [$image_id]);
      $image_entity = reset($image_entity);
      $image_entity = entity_metadata_wrapper(DRR_ENTITY_IMAGE,
        $image_entity, array('bundle' => DRR_BUNDLE_IMAGE));
      $image_entity->{DRR_PREFIX . $field} = $add_id;
      $image_entity->save();
    }
  }
}
