<?php

/**
 * @file
 * add administrative pages and forms
 */

/**
 * get the form data for basic readingroom settings
 * @param $form
 * @param $form_state
 * @return mixed
 */
function _readingroom_settings_form($form, &$form_state) {
  $form['maxAccessLevel'] = array(
    '#type' => 'textfield',
    '#title' => t('Public Access Levels'),
    '#description' => t('Data with this access level will be public.'),
    '#number_type' => 'integer',
    '#field_name' => 'num_seats',
    '#element_validate' => array('number_field_widget_validate'),
    '#field_parents' => array(),
    '#language' => NULL,
    '#size' => 2,
    '#maxlength' => 2,
    '#default_value' => variable_get(DRR_PREFIX . 'public_access_level', 2),
  );

  //include a submit button
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * submit function for basic readingroom settings form
 * @param $form
 * @param $form_state
 */
function _readingroom_settings_form_submit($form, &$form_state) {
  // check if public access level has changed
  if (check_plain($form_state['values']['maxAccessLevel']) != variable_get(DRR_PREFIX . 'public_access_level', 2)) {
    variable_set(DRR_PREFIX . 'public_access_level', check_plain($form_state['values']['maxAccessLevel']));
  }
  $form_state['redirect'] = '';
  return;
}
