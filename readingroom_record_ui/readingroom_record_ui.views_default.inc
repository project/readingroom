<?php

/**
 * @file
 * Provides default views for record part ofreadingroom solution
 */

/**
 * Implements: hook_views_default_views()
 * @return array
 */
function readingroom_record_ui_views_default_views() {
  $views = array();

  foreach (search_api_index_options_list() as $index_name => $info) {
    $index = search_api_index_load($index_name);
    if ($index) {
      if ($index->getEntityType() == DRR_ENTITY_ENTRY) {
        if (!isset($index->options['data_alter_callbacks']['search_api_alter_bundle_filter']['settings']['bundles'])) continue;
        $bundle = reset($index->options['data_alter_callbacks']['search_api_alter_bundle_filter']['settings']['bundles']);
        $options = array(
          'name' => $index_name . '_' . $bundle . '_view',
          'index' => $index_name,
          'bundle' => $bundle,
          'human name' => DRR_PREFIX_HUMAN . t('Entry') . '-' . t(substr($bundle, strlen(DRR_ENTITY_ENTRY)+1)),
        );
        $view_handler = new DrrViewHandler($options);

        $days = array();
        for ($i=1; $i<32; $i++) {
          $days[$i] = $i;
        }
        $months = array();
        $months_n = array(t('Jan'), t('Feb'), t('Mar'), t('Apr'), t('May'), t('Jun'), t('Jul'), t('Aug'), t('Sep'), t('Oct'), t('Nov'), t('Dec'), );
        for ($i=1; $i<13; $i++) {
          $months[$months_n[$i-1]] = $i;
        }
        $fields = array(
          DRR_PREFIX . 'record_ref_' . DRR_PREFIX . 'image_ref' => array(
            'exclude' => TRUE,
          ),
          DRR_PREFIX . 'person_ref_' . DRR_PREFIX . 'givenname' => array(
            'label' => 'Given Name',
            'search' => array(
              'exposed' => TRUE,
            ),
          ),
          DRR_PREFIX . 'person_ref_' . DRR_PREFIX . 'surname' => array(
            'label' => 'Last Name',
            'search' => array(
              'exposed' => TRUE,
            ),
          ),
          DRR_PREFIX . 'person_ref_' . DRR_PREFIX . 'person_role' => array(
            'label' => t('Role'),
            'link_to_entity' => 0,
            'view_mode' => 'full',
            'bypass_access' => 0,
            'search' => array(
              'exposed' => TRUE,
            ),
          ),
          DRR_PREFIX . 'event_ref_' . DRR_PREFIX . 'event_date' => array(
            'label' => 'Date',
          ),
          DRR_PREFIX . 'event_ref_' . DRR_PREFIX . 'event_day' => array(
            'exclude' => TRUE,
            'label' => t('Day'),
            'search' => array(
              'exposed' => TRUE,
              'entries' => $days,
            ),
          ),
          DRR_PREFIX . 'event_ref_' . DRR_PREFIX . 'event_month' => array(
            'label' => t('Month'),
            'exclude' => TRUE,
            'search' => array(
              'exposed' => TRUE,
              'entries' => $months,
            ),
          ),
          DRR_PREFIX . 'event_ref_' . DRR_PREFIX . 'event_year' => array(
            'label' => t('Year'),
            'exclude' => TRUE,
            'search' => array(
              'exposed' => TRUE,
            ),
          ),
          DRR_PREFIX . 'event_ref_' . DRR_PREFIX . 'location_name' => array(
            'label' => t('Location'),
            'link_to_entity' => 0,
            'search' => array(
              'exposed' => TRUE,
            ),
          ),
        );
        $view_handler->add_fields($fields);
        $views[$view_handler->get_name()] = $view_handler->get_view();
      }
    }
  }

  return $views;
}
