/**
 * @file
 * Utility Methods for the FsImageViewer
 */
var ImageViewerUtils = (function(window, $) {
  "use strict";

  /** Regex for recognizing deep zoom tiles **/
  var deepZoomTileRegex = /.+?_files(\/|%2F)\d+(\/|%2F)\d+_\d+\.(png|jpg)/;
  
  /** Regex for recognizing a deep zoom image **/
  var deepZoomRegex = /.+?\.(xml|dzi)$/;
  
  return {
    /**
     * Returns true if the specified image is from DAS
     * @param image to test
     * @returns true if the image is from DAS
     */
    isDasImage: function(image) {
        return image.indexOf('das/v2/') !== -1;
    },
    
    /**
     * Retrieves the source image from the thumbnail
     * This method is entirely based on naming conventions
     * @param thumbnail image to get the original image URL for
     * @return the original soure image URL
     */
    getSourceImageUrlFromThumbnail: function(thumbnail) {
        var imageUrl = thumbnail;
        var dasThumb = 'thumb_16.jpg';
        
        //if this is a deep zoom tile find the source
        if (thumbnail.match(deepZoomTileRegex)) {
            imageUrl = thumbnail.substring(0, thumbnail.indexOf('_files')) + '.xml';
        }
        
        //if this is a das thumbail
        else if (thumbnail.indexOf(dasThumb, thumbnail.length - dasThumb.length) !== -1) {
            imageUrl = thumbnail.replace(dasThumb, '$dist');
        }
        
        return imageUrl;
    },
    
    getImageThumbnailUrl: function(image) {
    
      var thumbnail = image;
      var isDas = ImageViewerUtils.isDasImage(image);
    
      //if the image is deep zoom
      if (image.match(deepZoomRegex)) {
        thumbnail = image.substr(0, image.lastIndexOf('.'));
        if (isDas) {
          thumbnail += '_files%2F7%2F0_0.jpg';
        } else {
          thumbnail += '_files/7/0_0.jpg';
        }
      }
    
      //if this is a jpeg image from DAS
      else if (isDas) {
        thumbnail = image.replace('$dist', 'thumb_16.jpg');
      }
      else {
//        thumbnail = image.replace('.jpg', '.thumb.jpg');
      }
      return thumbnail;
    },
    
    getImageUrl: function(tilesource) {
      if (typeof tilesource === 'string') return tilesource;
      return tilesource.levels[0].url;
    },
    
    getImageTileSource: function(imageUrl) {
      if (imageUrl.substr(-4).localeCompare('.xml') == 0) return imageUrl;
    alert("getImageTileSource: Url: " +imageUrl);
      var tileSource = {
              type: 'legacy-image-pyramid',
              levels:[{
                  url: imageUrl+".jpg",
                  height: 2338,
                  width:  1700
              }]
        }
      return tileSource;
    }

  };
  
  
  
}(window, jQuery));