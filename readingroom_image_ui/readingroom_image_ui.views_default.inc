<?php
/**
 * @file
 * administrative pages for readingroom_image_ui
 */

/**
 * Implements hook_views_defaull_views();
 * @return array
 */
function readingroom_image_ui_views_default_views() {
  $views = array();

  $options = array(
    'index' => DRR_BUNDLE_WAYPOINT . '_index',
    'bundle' => DRR_BUNDLE_WAYPOINT,
    'human name' => DRR_PREFIX_HUMAN . t('Waypoint'),
  );
  $view_handler = new DrrViewHandler($options);

  $fields = array(
    DRR_PREFIX . 'title' => array(
      'label' => 'Title',
      'search' => array(
        'exposed' => TRUE,
      ),
    ),
    DRR_PREFIX . 'subject' => array(
      'label' => 'Subject',
      'search' => array(
        'exposed' => TRUE,
      ),
    ),
    DRR_PREFIX . 'dates' => array(
      'label' => 'Dates',
    ),
    DRR_PREFIX . 'year_to' => array(
      'exclude' => TRUE,
      'label' => t('Year From'),
      'search' => array(
        'exposed' => TRUE,
        'operator' => '>=',
      ),
    ),
    DRR_PREFIX . 'year_from' => array(
      'exclude' => TRUE,
      'label' => t('Year To'),
      'search' => array(
        'exposed' => TRUE,
        'operator' => '<=',
      ),
    ),
  );

  $view_handler->add_fields($fields);
  $views[$view_handler->get_name()] = $view_handler->get_view();
  return $views;
}
