<?php
/**
 * @file
 * the file for DrrViewHandler
 */

/**
 * Class DrrViewHandler
 */
class DrrViewHandler {
  private $view;
  private $view_handler;  // view handler

  /**
   * @param array $options
   */
  function __construct($options) {
    $this->options = $options;
    $this->init_view();
  }

  /**
   * initialize a view
   */
  private function init_view() {
    $view = new view();
    $view->name = isset($this->options['name'])?$this->options['name']:$this->options['bundle'] . '_view';
    $view->description = '';
    $view->tag = 'default';
    $view->base_table = 'search_api_index_' . $this->options['index'];
    $view->human_name = $this->options['human name'];
    $view->core = 7;
    $view->api_version = '3.0';
    $view->disabled = FALSE;

    $this->view_handler = $view->new_display('default', 'Master', 'default');

    //set up the handler
    $this->view_handler->display->display_options['title'] = $this->options['human name'] . ' ' . t('View');
    $this->view_handler->display->display_options['access']['type'] = 'none';
    $this->view_handler->display->display_options['cache']['type'] = 'none';
    $this->view_handler->display->display_options['query']['type'] = 'views_query';
    $this->view_handler->display->display_options['exposed_form']['type'] = 'basic';
    $this->view_handler->display->display_options['exposed_form']['options']['submit_button'] = 'Search';
    $this->view_handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
    $this->view_handler->display->display_options['exposed_form']['options']['autosubmit'] = 0;
    $this->view_handler->display->display_options['exposed_form']['options']['autosubmit_hide'] = 1;
    $this->view_handler->display->display_options['pager']['type'] = 'full';
    $this->view_handler->display->display_options['pager']['options']['items_per_page'] = '20';
    $this->view_handler->display->display_options['style_plugin'] = 'table';
    $this->view_handler->display->display_options['style_options']['default'] = 1;
    $this->view_handler->display->display_options['style_options']['columns'] = array();
    $this->view_handler->display->display_options['style_options']['info'] = array();

    //provide default behavior if no records are available for viewing
    $this->view_handler->display->display_options['empty']['area']['id'] = 'area';
    $this->view_handler->display->display_options['empty']['area']['table'] = 'views';
    $this->view_handler->display->display_options['empty']['area']['field'] = 'area';
    $this->view_handler->display->display_options['empty']['area']['empty'] = FALSE;
    $this->view_handler->display->display_options['empty']['area']['content'] = t('No records available.');
    $this->view_handler->display->display_options['empty']['area']['format'] = 'plain_text';
    $this->view_handler->display->display_options['empty']['area']['tokenize'] = 0;

    $handler = $view->new_display('page', 'Page', 'page');
    $handler->display->display_options['path'] = $this->options['bundle'] . '_view';

    $this->view = $view;
    $this->add_field(
      array(
        'field_name' => 'id',
        'link_to_entity' => 0,
        'exclude' => TRUE,
      )
    );
  }

  /**
   * delivers the view handle within DRR_ViewHandler
   * @return view
   */
  public function get_view() {
    return $this->view;
  }

  public function get_name() {
    return $this->view->name;
  }

  /**
   * Add a list of fields into the view handled by DRR_ViewHandler
   * @param array $fields
   */
  public function add_fields($fields) {
    foreach ($fields as $id => $field) {
      $field['field_name'] = $id;
      $this->add_field($field);
    }
  }

  /**
   * Add a single field into the view
   * @param array $options
   */
  private function add_field($options) {
    $fieldName = $options['field_name'];
    $fieldLabel = isset($options['label'])?$options['label']:$fieldName;
    $tableName = $this->view->base_table;

    $this->view_handler->display->display_options['fields'][$fieldName] = array();
    $field = &$this->view_handler->display->display_options['fields'][$fieldName];
    $field['id'] = $fieldName;
    $field['field'] = $fieldName;
    $field['table'] = $this->view->base_table;
    foreach ($options as $key => $value) {
      if ($key == 'search') continue;
      $field[$key] = $value;
    }

    // set filters
    if (isset($options['search'])) {
      $this->view_handler->display->display_options['filters'][$fieldName] = array();
      $filter = &$this->view_handler->display->display_options['filters'][$fieldName];
      $filter['id'] = $fieldName;
      $filter['table'] = $tableName;
      $filter['field'] = $fieldName;

      $filter['expose']['operator_id'] = $fieldName . '_op';
      $filter['expose']['label'] = $fieldLabel;
      $filter['expose']['operator'] = $fieldName . '_op';
      $filter['expose']['identifier'] = $fieldName;
      $filter['expose']['required'] = 0;
      $filter['expose']['multiple'] = FALSE;
      $filter['expose']['remember_roles'] = array(
        2 => '2',
        1 => 0,
        3 => 0,
      );

      foreach ($options['search'] as $key => $value) {
        if ($key == 'entries') continue;
        $filter[$key] = $value;
      }

      if (isset($options['search']['entries'])) {
        $search_ops = array();
        $search_enties = &$options['search']['entries'];
        while ($value = current($search_enties)) {
          $search_ops[] = array(
            'title' => key($search_enties),
            'operator' => '=',
            'value' => array(
              'value' => $value,
              'min' => '',
              'max' => '',
            ),
          );
          next($search_enties);
        }
        $filter['is_grouped'] = TRUE;
        $filter['group_info']['label'] = $fieldLabel;
        $filter['group_info']['identifier'] = $fieldName;
        $filter['group_info']['group_items'] = $search_ops;
      }
    }
  }
}
