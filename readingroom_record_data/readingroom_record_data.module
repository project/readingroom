<?php

/**
 * @file
 * providing data structures for readingroom record solution
 */

// define the name of record entities
define('DRR_ENTITY_PERSON', DRR_PREFIX . 'person');
define('DRR_ENTITY_RECORD', DRR_PREFIX . 'records');
define('DRR_ENTITY_EVENT', DRR_PREFIX . 'events');
define('DRR_ENTITY_ENTRY', DRR_PREFIX . 'entry');

// define the names of tyxonomies
define('DRR_TAXONOMY_LOCATION', DRR_PREFIX . 'location');
define('DRR_TAXONOMY_PERSON_ROLE', DRR_PREFIX . 'person_role');

/**
 * load all taxonomies and terms defined in _readingroom_record_data_taxonomy_info()
 */
function _readingroom_record_data_taxonomy_load() {
  foreach (_readingroom_record_data_taxonomy_info() as $machine_name => $properties) {
    $taxonomy = taxonomy_vocabulary_machine_name_load($machine_name);
    if (!$taxonomy) {
      $properties['machine_name'] = $machine_name;
      taxonomy_vocabulary_save((object) $properties);
      $taxonomy = taxonomy_vocabulary_machine_name_load($machine_name);
    }

    if (isset($properties['vocabulary'])) {
      foreach ($properties['vocabulary'] as $term) {
        taxonomy_term_save((object) array(
          'name' => $term,
          'vid' => $taxonomy->vid,
        ));
      }
    }
  }
}

/**
 * remove all taxonomies and terms defined in _readingroom_record_data_taxonomy_info()
 */
function _readingroom_record_data_taxonomy_remove() {
  foreach (_readingroom_record_data_taxonomy_info() as $machine_name => $info) {
    $taxonomy = taxonomy_vocabulary_machine_name_load($machine_name);
    taxonomy_vocabulary_delete($taxonomy->vid);
  }
}

/**
 * define all taxonomies and its corresponding terms neccessary for readingroom_record
 * @return array
 */
function _readingroom_record_data_taxonomy_info() {
  return array(
    DRR_TAXONOMY_PERSON_ROLE => array(
      'name' => DRR_PREFIX_HUMAN . t('Person Role'),
      'description' => t('The role of a person within a record.'),
      'vocabulary' => array('father', 'mother', 'primary'),
    ),
    DRR_TAXONOMY_LOCATION => array(
      'name' => DRR_PREFIX_HUMAN . t('Location'),
      'description' => t('The name of the location where the event happened.'),
    ),
  );
}

/**
 * defines all entities with its corresponding properties, bundles, and fields
 * @return mixed
 */
function _readingroom_record_data_eck_entities_info() {
  $entities[DRR_ENTITY_PERSON] = array(
    'label' => DRR_PREFIX_HUMAN . t('Person'),
  );
  $entities[DRR_ENTITY_RECORD] = array(
    'label' => DRR_PREFIX_HUMAN . t('Record'),
  );
  $entities[DRR_ENTITY_EVENT] = array(
    'label' => DRR_PREFIX_HUMAN . t('Event'),
  );
  $entities[DRR_ENTITY_ENTRY] = array(
    'label' => DRR_PREFIX_HUMAN . t('Entry'),
  );
  return $entities;
}
