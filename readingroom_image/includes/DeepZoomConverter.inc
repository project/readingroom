<?php
/**
 * @file
 * file for class DeepZoomConverter
 */


/**
 * Class DeepZoomConverter
 * converts an image to DeepZoom format
 */
class DeepZoomConverter {

  /**
   * Convert an image to DeepZoom format
   * @param string $source_file image file name
   * @param string $dir image directory
   * @return string string name of DeepZoom xml file
   */
  public static function convert_image($source_file, $dir) {
    if (substr($source_file, strrpos($source_file, '.')) == '.xml') return $source_file;

    $file = $dir . '/' . $source_file;
    switch (pathinfo($file, PATHINFO_EXTENSION)) {
      case 'tif':
        $tif_image = new Imagick($file);
        $file = pathinfo($file, PATHINFO_DIRNAME) . '/' . pathinfo($file, PATHINFO_FILENAME) . ".jpg";
        $tif_image->writeImage($file);
        $tif_image->clear();
        break;

      case 'pdf':
        $pdf_image = new imagick();
        $pdf_image->setResolution(300, 300);
        $pdf_image->readimage($file);
        $pdf_image->setImageFormat('jpeg');
        $file = pathinfo($file, PATHINFO_DIRNAME) . '/' . pathinfo($file, PATHINFO_FILENAME) . ".jpg";
        $pdf_image->writeImage($file);
        $pdf_image->clear();
        $pdf_image->destroy();
        break;

      case 'jpg':
        break;

      default:
        return $source_file;  // don't convert any other format
    }

    $tile_size = 256;
    $overlap = 1;
    $filename = pathinfo($file, PATHINFO_FILENAME);

    $level_image = imagecreatefromjpeg($file);
    $config = array(
      'TileSize' => $tile_size,
      'Overlap' => $overlap,
      'Format' => 'jpg',
      'ServerFormat' => 'Default',
      'xmlns' => 'http://schemas.microsoft.com/deepzoom/2009',  // TODO: is this needed?
      'Size' => array(
        'Width' => imagesx($level_image),
        'Height' => imagesy($level_image),
      ),
    );
    self::writeXml("{$dir}/{$filename}.xml", array('Image' => $config));

    $num_levels = (int)ceil(log(max(imagesx($level_image), imagesy($level_image)), 2));
    $levels = array();
    foreach (range(0, $num_levels) as $level) {
      $levels[$level] = array();
      $current = &$levels[$level];
      $scale = pow(0.5, $num_levels - $level);
      $current['width'] = (int)ceil($config['Size']['Width'] * $scale);
      $current['height'] = (int)ceil($config['Size']['Height'] * $scale);
      $current['columns'] = (int)ceil(floatval($current['width']) / $tile_size);
      $current['rows'] = (int)ceil(floatval($current['height']) / $tile_size);
    }

    for ($level = $num_levels; $level >= 0; $level--) {
      $level_dir = "{$dir}{$filename}_files/{$level}";
      if (!file_exists($level_dir)) self::create_dir($level_dir);

      $cur_lev = $levels[$level];

      if ($level < $num_levels) {
        $help = imagecreatetruecolor($cur_lev['width'], $cur_lev['height']);
        imagecopyresized($help, $level_image, 0, 0, 0, 0, $cur_lev['width'], $cur_lev['height'], imagesx($level_image), imagesy($level_image));
        $level_image = imagecreatetruecolor($cur_lev['width'], $cur_lev['height']);
        imagecopy($level_image, $help, 0, 0, 0, 0, $cur_lev['width'], $cur_lev['height']);
        unset($help);
      }

      for ($row = 0; $row < $cur_lev['rows']; $row++) {
        for ($col = 0; $col < $cur_lev['columns']; $col++) {
          if (!file_exists("{$level_dir}/{$col}_{$row}.jpg")) {
            $offset_x = $col == 0 ? 0 : $overlap;
            $offset_y = $row == 0 ? 0 : $overlap;
            $x = ($col * $tile_size) - $offset_x;
            $y = ($row * $tile_size) - $offset_y;

            $tile_width = min($tile_size + ($col == 0 ? 1 : 2) * $overlap, $levels[$level]['width'] - $x);
            $tile_height = min($tile_size + ($row == 0 ? 1 : 2) * $overlap, $levels[$level]['height'] - $y);

            $tile_image = imagecreatetruecolor($tile_width, $tile_height);
            imagecopy($tile_image, $level_image, 0, 0, $x, $y, $tile_width, $tile_height);
            imagejpeg($tile_image, "{$level_dir}/{$col}_{$row}.jpg", 25);
            unset($tile_image);
          }
        }
      }
    }
    file_unmanaged_delete($file);
    return "{$filename}.xml";
  }

  /**
   * append an array
   * @param DOMDocument $parent
   * @param $array
   * @param null $doc
   */
  private static function append_array(DOMNode &$parent, $array, $doc = NULL) {
    if (!is_array($array)) return;
    if ($doc == NULL) $doc = $parent;
    foreach ($array as $key => $value) {
      if (is_array($value)) {
        $ele = $doc->createElement($key);
        self::append_array($ele, $value, $doc);
        $parent->appendChild($ele);
      }
      else {
        $parent->setAttribute($key, $value);
      }
    }
  }
  /**
   * create an directory
   * @param $dir_name
   */
  private static function create_dir($dir_name) {
    $parent = dirname($dir_name);
    if (!file_exists($parent)) self::create_dir($parent);
    mkdir(str_replace('/', DIRECTORY_SEPARATOR, $dir_name));
  }

  /**
   * write the xml-file for DeepZoom image
   * @param $file
   * @param $array
   */
  private static function writeXml($file, $array) {
    $doc = new DOMDocument('1.0', 'utf-8');
    self::append_array($doc, $array);
    $doc->save($file);
  }

}
