<?php

/**
 * @file
 * The DrrBaseImporter class file.
 */

// DrrBaseImporter class ######################################################

/**
 * class providing base methods for data imports
 * @author BeyersdorferTJ
 */
class DrrBaseImporter implements IDrrCsvImporter {
  public static $log_writer;     // log-Writer
  protected static $watchdog_type;
  public static $params;      // initialisation parameters
  private static $start_time;    // time when the import has been started
  private static $import_time;   // time when import has been finished
  private static $connect_time;  // time when records and images have been connected
  private static $import_id;      // import ID

  /**
   * @param $params array initialisation parameters
   * array(
   *   'base dir',      // string - import root directory (mandatory)
   *   'logfile'        // string - complete path to the logfile
   *   'truncate'       // boolean - set if the data should be deleted before import
   *   'image convert'  // boolean - set if images should be converted to DeepZoom format before import
   *   'type'           // string - import type (all | records | images),
   * );
   */
  function __construct($params, $message = 'Import started') {
    self::$watchdog_type = DRR_PREFIX . 'importer';
    self::$params = $params;
    self::$params['doc root'] = apache_getenv("DOCUMENT_ROOT") . DIRECTORY_SEPARATOR;
    self::$import_id = 0;
    self::$start_time = time();
    self::$import_time = self::$start_time;
    self::$connect_time = self::$start_time;
    $this->watchdog($message);
  }

  /**
   * dummy function to do real csv import work
   * should be overridden in child class
   */
  protected function execute_csv_import(IDrrCsvImporter $imp) {
  }

  /**
   * set the import to status "connected"
   */
  public static function finish_connect() {
    if (self::$import_id > 0) {
      self::$connect_time = time() - self::$start_time - self::$import_time;
      $formated_connect_time = DrrBaseImporter::format_time(self::$connect_time);
      DrrBaseImporter::set_status('finished');
      DrrBaseImporter::watchdog('Data Connect finished -> Duration: @duration', array('@duration' => $formated_connect_time['text']));
      $formated_total_time = DrrBaseImporter::format_time(time() - self::$start_time);
      DrrBaseImporter::watchdog('Import Finished -> Total Duration: @duration', array('@duration' => $formated_total_time['text']));
    }
  }

  /**
   * set the import to status "imported"
   */
  public static function finish_import() {
    if (self::$import_id > 0) {
      self::$import_time = time() - self::$start_time;
      $formated_startTime = DrrBaseImporter::format_time(self::$import_time);
      DrrBaseImporter::set_status('imported');
      DrrBaseImporter::watchdog('Data Import finished -> Duration: @duration', array('@duration' => $formated_startTime['text']));
    }
  }

  /**
   * Format the timestamp to print out duration
   * @param $time int timestamp $time
   * @return array of strings for sec, min, hour
   */
  private static function format_time($time) {
    $formatted = array();
    $formatted['sec'] = $time % 60;
    $time = ($time - $formatted['sec']) / 60;
    $formatted['min'] = $time % 60;
    $formatted['hour'] = ($time - $formatted['min']) / 60;
    $formatted['text'] = '' . ($formatted['hour'] > 0 ? ($formatted['hour'] . ' ' . t('hours') . '  ') : '') .
      ($formatted['min'] > 0 ? ($formatted['min'] . ' ' . t('minutes') . '  ') : '');
    $formatted['text'] .= ($formatted['sec'] > 0 || strlen($formatted['text']) == 0 ? ($formatted['sec'] > 0 ? $formatted['sec'] : '0') . ' ' . t('seconds') . ' ' : '');
    return $formatted;
  }

  /**
   * dummy function to deliver deliver CsvLoader
   * @param $type
   * @return null
   */
  protected function get_csv_loader($type, $dir, $file, $fileID) {
    return new DrrDummyCsvLoader();
  }

  /**
   * get the ID for the current import
   */
  public function get_import_id() {
    if (self::$import_id > 0) return self::$import_id;
    return self::new_import();
  }

  /**
   * getr last watchdog message
   */
  public static function get_last_watchdog_message() {
    $query = db_select('watchdog');
    $query->addExpression('MAX(wid)');
    $max_wid = $query->execute()->fetchField();

    return db_select('watchdog', 'w')
      ->fields('w', array('message'))
      ->condition('wid', $max_wid)
      ->execute()
      ->fetchField();
  }

  /**
   * check if a file has already been imported
   * @param String - md5 hash of the file
   * @return boolean
   */
  protected function has_file_imported($md5) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', DRR_ENTITY_IMPORT_FILES)
      ->fieldCondition(DRR_PREFIX . 'md5', 'value', $md5, '=');

    $result = $query->execute();
    if (!$result) return FALSE;
    return TRUE;
  }

  /**
   * import a CSV-file
   * @param $dir
   * @param $filename
   * @param $module String - module nameto handle imports for
   */
  protected function import_csv_file($dir, $file, $module) {
    $full_path = self::$params['doc root'] . $dir . $file;
    $fh = fopen($full_path, 'r');
    if (!$fh) return;  // access denied
    try {
      // read first line and handle it
      if (($line = fgets($fh)) == FALSE) { // empty file
        self::watchdog('Skipping empty file @filename', array('@filename' => $full_path), WATCHDOG_WARNING);
        throw new DrrInterruptException;
      }
      if ($line[0] != '#') {  // first char has to be '#'; rest json
        self::watchdog('Invalid format. Missing leading "#" in line 1 in @filename', array('@filename' => $full_path), WATCHDOG_ALERT);
        throw new DrrInterruptException;  // first char has to be '#'; rest json
      }
      $json = json_decode(substr($line, 1));
      if (!array_key_exists('module', $json)) {
        self::watchdog('Invalid json format. Missing module name in @filename', array('@filename' => $full_path), WATCHDOG_ALERT);
        throw new DrrInterruptException;
      }
      if (!array_key_exists('type', $json)) {
        self::watchdog('Invalid json format. Missing type name in @filename', array('@filename' => $full_path), WATCHDOG_ALERT);
        throw new DrrInterruptException;
      }
      if ($json->module != $module) throw new DrrInterruptException;

      $md5 = md5_file($full_path);
      if ($this->has_file_imported($md5)) throw new DrrInterruptException;

      // file needs to be imported
      self::watchdog('Start to import file @filename', array('@filename' => $full_path));
      $file_id = $this->mark_file_as_imported($dir . $file, $md5);
      fclose($fh);

      $this->execute_csv_import($this->get_csv_loader($json->type, $dir, $file, $file_id));
      self::watchdog('File import finished of @filename', array('@filename' => $full_path));
    } catch (DrrInterruptException $e) {
      fclose($fh);
    }
  }

  /**
   * create a new import entry
   */
  private function new_import() {
    $time = time();
    $import = readingroom_entity_create(DRR_ENTITY_IMPORT, DRR_BUNDLE_IMPORT);
    $import->{DRR_PREFIX . 'status'} = 'started';
    $import->{DRR_PREFIX . 'start_time'} = $time;
    $import->{DRR_PREFIX . 'status_time'} = $time;
    $import->save();
    self::$import_id = $import->id->value();
    return self::$import_id;
  }
  /**
   * mark a file as imported
   * @param $filename String - path to the file to be imported
   * @param $md5 String - md5 hash of the file
   * @return integer - fileID
   */
  protected function mark_file_as_imported($filename, $md5) {
    $time = time();
    $file_entity = readingroom_entity_create(
      DRR_ENTITY_IMPORT_FILES, DRR_BUNDLE_IMPORT_FILE);
    /* @var $file_entity EntityMetadataWrapper */
    $file_entity->{DRR_PREFIX . 'filename'} = $filename;
    $file_entity->{DRR_PREFIX . 'md5'} = $md5;
    $file_entity->{DRR_PREFIX . 'import_ref'} = $this->get_import_id();
    $file_entity->{DRR_PREFIX . 'start_time'} = $time;
    $file_entity->{DRR_PREFIX . 'status_time'} = $time;
    $file_entity->{DRR_PREFIX . 'status'} = 'started';
    $file_entity->save();
    return $file_entity->id->value();
  }

  /**
   * Set a new Status for the import running
   * @param String $status new status of the import
   */
  public static function set_status($status) {
    if (self::$import_id > 0) {
      $import_entity = entity_load_single(DRR_ENTITY_IMPORT, self::$import_id);
      $wrapper = entity_metadata_wrapper(
        DRR_ENTITY_IMPORT, $import_entity,
        array('bundle' => DRR_BUNDLE_IMPORT)); /* @var $wrapper EntityDrupalWrapper */
      $wrapper->{DRR_PREFIX . 'status'} = $status;
      $wrapper->{DRR_PREFIX . 'status_time'} = time();
      $wrapper->save();
    }
  }

  /**
   * initiate a watchdog message
   * @param $message
   * @param array $replacments
   * @param int $type
   */
  public static function watchdog($message, $replacments = array(), $type = WATCHDOG_NOTICE) {
    watchdog(self::$watchdog_type, $message, $replacments, $type);
  }

  public function start_import() {
    // Dummy function, should be overwritten by child class
  }
}
