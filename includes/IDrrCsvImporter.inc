<?php

/**
 * @file
 * The ICsvImporter interface file.
 */

/**
 * Interface IDrrCsvImporter
 */
Interface IDrrCsvImporter {
  public function start_import();
}
