<?php

/**
 * @file
 * The DrrInterruptException class file.
 */

/**
 * DrrInterruptException class
 *
 * this class is used to catch an interrupt
 */
class DrrInterruptException extends Exception {}
