/**
 * @file Javascript/AJAX image viewer for deepzoom images
 */

var ImageViewer = (function(window, $) {
  "use strict";

  var ImageViewer = function(divId, image) {
    this.viewer = null;
    this.parrentDiv = $('#' + divId);
    
    this.viewerDiv = this.parrentDiv.append($('<div id="seadragonViewer" class="seadragonViewer"></div>')).children().last();
    this.viewer = new Seadragon.Viewer(this.viewerDiv.attr("id"));
    this.viewer.setDashboardEnabled(false);  // disable seadragon buttons - TJB
    this.switchImage(image);
  }
  
  ImageViewer.prototype.switchImage = function(image) {
    /*
    if (!image) this.viewer.close();
    else {
      if (endsWith(image, '.xml')) {
        this.viewer.openDzi(image);
      } else {
//        this.viewer.openDzi(image);
        var jpegImg = document.createElement('img');
        var myViewer = this;
        Seadragon.Utils.addEvent(jpegImg, "load", function() {
//          myViewer.viewer.openTileSource(new ImageTileSource(
//              jpegImg.width, jpegImg.height, jpegImg.src));
//          myViewer.Utils.openTileSource(Seadragon.TileSource(jpegImg.width, jpegImg.height, Math.max(jpegImg.width, jpegImg.height), 1, 12, 12));
        });
        jpegImg.src = image;
      }
    }
         */
  }
  
  /**
   * Ends with function from http://stackoverflow.com/questions/280634/endswith-in-javascript
   */
  function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
  }
  
  return ImageViewer;
}(window, jQuery));

var initViewer = (function(window, $) {
  "use strict";

  var initViewer = function(options) {
    var parrent = $("#" + options.div);
    if (parrent.length==0) throw new Error("Div \"" + options.div + "\" hasn't found. Unable to open DRR Viewer.");
    
    if (!options.hasOwnProperty('id')) options.id = 'seadragon_container';
    parrent.append('<div id="' + options.id + '"></div>').children().last();
    
    options.prefixUrl = options.module_path + '/js/openseadragon-bin-1.0.0/images/';
    if (!options.hasOwnProperty('selected')) options.selected = 0;
    if (options.selected<0 || options.selected >= options.images.length) options.selected = 0;
    options.tileSources = options.images[options.selected];
  
    options.viewer = OpenSeadragon(options);

    if (!options.hasOwnProperty('showGallery')) options.showGallery = true;
    if (options.showGallery) {
      options.galleryContainer = parrent.append('<div id="gallery_container"></div>').children().last();
      setTimeout(function(){GalleryPanel2(options);}, 1000);
    }

//  OpenSeadragon({
//    id:            "drr_js_viewer",
//    prefixUrl:     "C:/Program Files/Zend/Apache2/htdocs/DRR-Devel/sites/default/modules/Repository/ReadingRoom/readingroom_viewer_js/openseadragon-bin-0.9.127/images",
//    tileSources:   "http://localhost/deepzoom/004202352_00001.xml",
//    visibilityRatio: 1.0,
//    constrainDuringPan: true
  //});

//  var imageViewer = new ImageViewer(divId, image);
//  return imageViewer;
};
return initViewer;
}(window, jQuery));

// old staff ##################################################################

/**
 * Create a tile source that allow for the viewing of flat JPEG or PNG images
 * Taken from https://getsatisfaction.com/livelabs/topics/seadragon_ajax_deepzoom_small_images
 * @param width
 * @param height
 * @param src
 */
function ImageTileSource(width, height, src) {

    // Inheritance
    Seadragon.TileSource.apply(this, [width, height, Math.max(width, height)]);

    // Overridden properties
    this.minLevel = this.maxLevel;

    // Overridden methods
    this.getTileUrl = function(level, x, y) {
        // assuming the only time this will be called will be for maxLevel, 0, 0
        return src;
    };
    
    this.tileExists = function(level, x, y) {
        // assuming the only time this will be called will be for maxLevel, 0, 0
        return true;
    };
}
// ImageTileSource.prototype = new Seadragon.TileSource();

var ImageViewerJS = (function(window, $) {
  "use strict";

  /**
   * Constructs a new ImageViewer
   * 
   * @param divId
   *            id of the div to load the viewer into
   * @param image
   *            to load
   */
  var ImageViewerJS = function(divId, image) {

    // Publicly accessible seadragon viewer
    this.seadragonViewer = null;

    // Publicly accessible viewer div
    this.viewerDiv = $('#' + divId);
     
    // Publicly accessible array of domains to encode slashes for
    this.viewImage;
    
    this.seadragonDiv = this.viewerDiv.append('<div id="seadragonViewer" class="seadragonViewer"></div>').children().last();
    this.seadragonViewer = new Seadragon.Viewer(this.seadragonDiv.attr("id"));
    this.seadragonViewer.setDashboardEnabled(false);  // disable seadragon buttons - TJB
    this.switchImage(null, image);
    //updateSelectModeEventHandlers(this);
    // this.seadragonViewer.addEventListener('animation',
    // applyImageManipulations);
    // this.seadragonViewer.addEventListener('redraw',
    // applyImageManipulations);
  };

  ImageViewerJS.prototype.switchImage = function(event, image) {
    this.viewImage = null;
    if (image) {
      if (endsWith(image, '.xml')) {
        this.viewImage = image;
        this.seadragonViewer.openDzi(image);
      } else {
        if (!this.viewImage) {
          var jpegImg = document.createElement('img');
          var viewer = this;
          Seadragon.Utils.addEvent(jpegImg, "load", function() {
            viewer.seadragonViewer.openTileSource(new ImageTileSource(
                jpegImg.width, jpegImg.height, jpegImg.src));
          });

          jpegImg.src = image;
          this.viewImage = image;
        }
      }

    } else {
      this.seadragonViewer.close();
    }
    if (event) {
      Seadragon.Utils.cancelEvent(event);
    }
  };

  /**
   * Ends with function from http://stackoverflow.com/questions/280634/endswith-in-javascript
   */
   function endsWith(str, suffix) {
       return str.indexOf(suffix, str.length - suffix.length) !== -1;
   }

  return ImageViewerJS;

}(window, jQuery));

/**
 * Function for initializing the viewer Call this instead of invoking the
 * FsImageViewer directly to ensure proper event handling
 * 
 * @param divId
 *            id of the div for the seadragon viewer
 * @param image
 *            to initially open the viewer with
 * @param callback
 *            function for customizing the image viewer; the initialized
 *            FsImageViewer will be passed as the first argument
 */

var initViewerJS = function(divId, imageSelected, callback) {
  var imageViewerJS = null;
  var loaded = false;

  imageViewerJS = new ImageViewerJS(divId, imageSelected);
//  imageViewerJS.seadragonViewer.addEventListener("open", function() {
  imageViewerJS.seadragonViewer.addHandler("open", function() {
  if (!loaded) {
    imageViewerJS.seadragonViewer.removeEventListener("open");
    callback(imageViewerJS);
  }
//  loaded = true; //TODO: can we remove this event listener so it no longer
  // gets called after the first initialization
  });
};



