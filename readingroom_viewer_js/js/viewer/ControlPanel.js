/**
 * @file
 * Provides a class for constructing control panels on the image viewer
 */
var ControlPanel = (function(window, $) {
  "use strict";

  
  var ControlPanel = function(parentElem, fsImageViewer, overrides) {
    
    var controlPanelDiv = parentElem.append('<div id="control_panel" class="control_panel default_size"></div>').children().last();
    var imgFuncDiv = controlPanelDiv.append('<div id="imgFunc"></div>').children().last();
    
    //default buttons to create;
    var buttons = {
      fullscreen: {
        css: 'fsbutton fullscreen',
        label: 'Full Screen',
        action: function() { toggleFullScreen(); $("#gallery_panel").height(screen.height);},
        supported: isFullScreenSupported
      }
    };
 
    //create the buttons
    for (var buttonKey in buttons) {
      var button = buttons[buttonKey];
      (function(button, parentElem){
        var buttonElement = $('<div><a class="' + button.css + '"><span class="buttonText">' + button.label + '</span><span class="spacer">&nbsp;</span></a></div>');
          if (button.supported()) {
//            alert("supported");
              buttonElement.click(button.action);
          }
          else {
//            alert("not supported");
              buttonElement.children().first().addClass('disabled_fsbutton');
          }
//          alert("Test");
          parentElem.append(buttonElement);
      }(button, imgFuncDiv));
  }
          

  };
  
  /**
   * Returns true if the browser supports full screen mode
   */
  function isFullScreenSupported() {
      var docElm = document.documentElement;
      if (docElm.requestFullscreen) {
//        alert("1"); 
      } else if (docElm.mozRequestFullScreen) {
//        alert("2");
      } else if (docElm.webkitRequestFullscreen) {
//        alert("3"); 
      } 

      return (docElm.requestFullScreen || docElm.mozRequestFullScreen || docElm.webkitRequestFullScreen || docElm.msRequestFullScreen);
  };

  /**
   * Toggles full screen mode
   * Code primarily taken from http://stackoverflow.com/questions/1125084/how-to-make-in-javascript-full-screen-windows-stretching-all-over-the-screen 
   */
  function toggleFullScreen() {
    var isInFullScreen = (document.fullScreenElement && document.fullScreenElement !== null) ||
                         (document.mozFullScreen || document.webkitIsFullScreen);
    if (isInFullScreen) {
//      alert("switch full screen off");
      var elem = document;
      var requestMethod = elem.cancelFullScreen ||
                          elem.webkitCancelFullScreen ||
                          elem.mozCancelFullScreen ||
                          elem.exitFullscreen;
      requestMethod.call(elem);
    } else {
//      alert("switch full screen on");
      var elem = document.documentElement;
      var requestMethod = elem.requestFullScreen || elem.webkitRequestFullScreen || elem.mozRequestFullScreen ||elem.msRequestFullScreen;
      requestMethod.call(document.documentElement);
    }
  };
  
  //return the constructor
  return ControlPanel;
  
}(window, jQuery));