/**
 * @file Provides a constructor gallery panels on the image viewer TODO: revisit
 *       how set the current image selected, is this the way we want to do it
 *       TODO: are we doing more jquery selections than we need to on image
 *       switches; can we make the code more efficient TODO: pull sliding seems
 *       to work most of the time but there still appears to be some bugs
 */
var GalleryPanel2 = (function(window, $) {
  function switchImage(image_id) {
    if (image_id) {
      $('#selectedThumbnail').removeAttr('id');
      var img = $('#thumbnail_strip').find(
          'img.thumbnail[image_id=\'' + image_id + '\']').attr('id',
          'selectedThumbnail');
//      var help = 
//      $(this).find('img.thumbnail').attr('src');

//      this.options.viewer.open(ImageViewerUtils.getImageTileSource(thumbnailUrl));
      this.options.viewer.open(this.options.images[image_id]);
    }
  }

  var GalleryPanel2 = function(options) {
    this.options = options;
    var table = options.galleryContainer.append('<div id="gallery_panel"></div>')
    .children().last().append('<div id="thumbnail_strip"></div>')
    .children().last().append('<table class="thumbnail_table"></table>')
    .children().last();
    
    // insert images
    var tableContent = '';
    for (var i=0; i < options.images.length; i++) {
      var imageUrl = ImageViewerUtils.getImageUrl(options.images[i]);
      tableContent += '<tr><td>' + (i + 1)
        + '</td><td class="thumbnail_column"><img class="thumbnail" src="'
        + ImageViewerUtils.getImageThumbnailUrl(imageUrl)
        + '" image_id="' + i
//        + '" file="' + imageUrl
        + '"></td></tr>';
      /*
      */
    }
    table.append(tableContent);
    
    // set valid selection
    var img = table.find('tr:nth-child(' + (options.selected + 1) + ')').find('img.thumbnail').attr('id', 'selectedThumbnail');

    var imagesLoaded = 0;
    $('#thumbnail_strip tr td img').load(function() {
      imagesLoaded++;
      if (imagesLoaded === options.images.length) {
        $('#thumbnail_strip').scrollTop(img.position().top);
      }
    });

    // set up click functionality
    var galleryPanel = this;
    $('#thumbnail_strip tr').click(function() {
      var thumbnailUrl = $(this).find('img.thumbnail').attr('image_id');
      switchImage(thumbnailUrl);
    });
    /*
    */
//    options.viewer.open(options.images[1]);
  }
  
  return GalleryPanel2;
}(window, jQuery));

var GalleryPanel = (function(window, $) {
  "use strict";

  /**
   * Indicates how the image is being switched 'no_zoom' indicates open with no
   * zoom 'click_zoom' indicates open with previous zoom saved 'pull_zoom'
   * indicates open with previous zoom on correct portion of the document Any
   * other value defaults to 'no_zoom'
   */
  var imageSwitchType = null;

  /**
   * Storage for last record bounds of the previous image
   */
  var lastRecordedBounds = null;

  /**
   * Switches the image in the viewer to the image represented by the thumbnail
   * URL
   * 
   * @param imageViewer
   *            to switch images for
   * @param thumbnailUrl
   *            to switch to
   * @param autoscroll?
   *            true if the gallery should autoscroll, false by default
   */
  function switchImage(imageViewer, thumbnailUrl, autoscroll) {
    if (thumbnailUrl) {
//      var image = ImageViewerUtils.getSourceImageUrlFromThumbnail(thumbnailUrl);
      $('#selectedImage').removeAttr('id');
      var img = $('#thumbnail_strip').find(
          'img.thumbnail[file=\'' + thumbnailUrl + '\']').attr('id',
          'selectedImage');
      if (autoscroll) {
        $('#thumbnail_strip').scrollTop(img.position().top);
      }
      lastRecordedBounds = imageViewer.seadragonViewer.viewport.getBounds();
      imageViewer.switchImage(null, thumbnailUrl);
    }
  }
  ;

  /**
   * Returns the index of the currently selected index
   */
  function getCurrImageIndex() {
    return parseInt($('#selectedImage').parent().siblings().first().text());
  }

  /**
   * Returns the URL of the previous image thumbnail or null if there is no
   * previous image
   */
  function getPreviousImageThumbnailUrl() {
    var prevImage = null;
    var index = getCurrImageIndex();
    if (index !== 1) {
      prevImage = $('#thumbnail_strip').find(
          'tr:nth-child(' + (index - 1) + ')').find('img.thumbnail')
          .attr('src');
    }
    return prevImage;
  }
  ;

  /**
   * Return the URL of the next image thumbnail or null if there is no next
   * image
   */
  function getNextImageThumbnailUrl() {
    var nextImage = null;
    var index = getCurrImageIndex();
    var nextImageNode = $('#thumbnail_strip').find(
        'tr:nth-child(' + (index + 1) + ')').find('img.thumbnail');
    if (nextImageNode.length === 1) {
      nextImage = nextImageNode.attr('src');
    }
    return nextImage;
  }
  ;

  /**
   * Reconstructs the gallery with settings based on the current window size
   * 
   * @param imageViewer
   * @param galleryContainer
   * @param galleryPanelDiv
   * @param thumbnailHandle
   */
  function reconstructGallery(imageViewer, galleryContainer, galleryPanelDiv,
      thumbnailHandle) {

    var viewerOffset = imageViewer.viewerDiv.offset();
    var viewerWidth = imageViewer.viewerDiv.width();
    var viewerHeight = imageViewer.viewerDiv.height();
    var thumbnailHandleWidth = 17;

    // if we are sizing down reset the size of the gallery and the positions of
    // the gallery and he thumbnail
    var maxGalleryWidth = (viewerWidth / 2.0);
    var rightContainment = viewerOffset.left + viewerWidth
        - thumbnailHandleWidth;
    var leftContainment = rightContainment - maxGalleryWidth;
    if (galleryPanelDiv.width() > maxGalleryWidth) {
      galleryPanelDiv.width(rightContainment + thumbnailHandleWidth
          - maxGalleryWidth);
      thumbnailHandle.offset({
        left : leftContainment
      });
    }
    galleryPanelDiv.data('width', galleryPanelDiv.width());

    // determine the bounds and set variables base on current size
    thumbnailHandle.css('top', Math.max(((($(window).height() - 72) / 2) - 70),
        0));

    // TODO: this is not an ideal solution in my mind, but this is the only way
    // I could get it to work
    // prior to this we had issues with whe resizing the window
    /*
     * thumbnailHandle.draggable('destroy'); thumbnailHandle.draggable({ axis:
     * 'x', drag: function(event, ui) { var diff = ui.originalPosition.left -
     * ui.position.left; var newWidth = galleryPanelDiv.data('width') + diff;
     * galleryPanelDiv.width(newWidth); }, stop: function(event, ui) {
     * galleryPanelDiv.data('width', galleryPanelDiv.width()); }, containment:
     * [leftContainment, viewerOffset.top, rightContainment, viewerOffset.top +
     * viewerHeight] });
     */
  }
  ;

  /**
   * Constructor for gallery panel
   * 
   * @param parentElement
   *            to add the gallery panel to
   * @param imageViewer
   *            to construct the gallery panel for
   * @param images
   *            to load in the gallery
   * @param selectedIndex
   *            the index of the image in the array that is initally selected,
   *            defaults to 0 the gallery panel does not switch to the selected
   *            index supplied, but simply marks the index in the gallery as the
   *            selected image it is assumed that the viewer already has the
   *            selected image in view before constructing the gallery panel
   */
  var GalleryPanel = function(parentElement, imageViewer, images,
      selectedIndex) {
    // set the variables
    this.imageViewer = imageViewer;
    // append the structure for the gallery panel
    var galleryContainer;
    if ($("#gallery_container").length > 0) return;  // panels already exist
    var galleryContainer = parentElement.append(
        '<div id="gallery_container"></div>').children().last();
    var thumbnailHandle = galleryContainer.append(
      '<div id="thumbnail_handle"></div>').children().last();
    var galleryPanelDiv = galleryContainer.append(
        '<div id="gallery_panel" class="gallery_panel"></div>').children().last();
    var table = galleryPanelDiv.append('<div id="thumbnail_strip"></div>')
        .children().last().append('<table class="thumbnail_table"></table>')
        .children().last();

    // insert the images
    var tableContent = '';
    for ( var i = 0; i < images.length; i++) {
      tableContent += '<tr><td>' + (i + 1)
          + '</td><td><img class="thumbnail" src="'
          + ImageViewerUtils.getImageThumbnailUrl(images[i])
          + '" file="' + images[i] + '"></td></tr>';
    }
    table.append(tableContent);

    // set valid selection
    if (typeof selectedIndex !== 'number' || selectedIndex < 0
        || selectedIndex >= images.length) {
      selectedIndex = 0;
    }

    // TODO: may need to revisit this once we have images loading dynamically on
    // demand
    var img = table.find('tr:nth-child(' + (selectedIndex + 1) + ')').find('img.thumbnail').attr('id', 'selectedImage');
    
    var imagesLoaded = 0;
    $('#thumbnail_strip tr td img').load(function() {
      imagesLoaded++;
      if (imagesLoaded === images.length) {
        $('#thumbnail_strip').scrollTop(img.position().top);
      }
    });

    // set up the gallery to survive window resize
    $(window).resize(
        function() {
          galleryPanelDiv.height($(window).height()-20);
          reconstructGallery(imageViewer, galleryContainer, galleryPanelDiv,
              thumbnailHandle);
        });

    // set up the gallery panel functionality
    var w_height = $(window).height();
    galleryPanelDiv.height(w_height -20);
    galleryPanelDiv.data('width', galleryPanelDiv.width());

    // set up click functionality
    var galleryPanel = this;
    $('#thumbnail_strip tr').click(function() {
      var thumbnailUrl = $(this).find('img.thumbnail').attr('file');
      galleryPanel.clickSwitchImage(thumbnailUrl);
    });
    
    /*
    // construct the draggable gallery
    reconstructGallery(imageViewer, galleryContainer, galleryPanelDiv,
        thumbnailHandle);

    // set up pulling transitions
    // store the original drag handler (original to us)
    var origDragHandler = imageViewer.seadragonViewer.tracker.dragHandler;

    // store the original release handler
    var origReleaseHandler = imageViewer.seadragonViewer.tracker.releaseHandler;

    // keep track of whether we are in a drag or not
    var inDrag = false;

    // update the release handler such that we know when a drag has stopped
    // this is necessary to avoid a drag to next/prev image switching multiple
    // images instead of just one
    imageViewer.seadragonViewer.tracker.releaseHandler = function(tracker,
        position, insideElmtPress, insideElmtRelease) {
      inDrag = false;
      origReleaseHandler(tracker, position, insideElmtPress, insideElmtRelease);
    };

    // update the drag handler for transitions
    imageViewer.seadragonViewer.tracker.dragHandler = function(tracker,
        position, delta) {
      if (imageViewer.seadragonViewer.viewport) {

        // determine the viewport location
        var viewportLocation = imageViewer.seadragonViewer.viewport
            .pointFromPixel(new Seadragon.Point(parseInt(position.x),
                parseInt(position.y)));
        // var negRotate = Seadragon.Utils.getReverseRotation(rotationDegrees);
        // var viewportCenter =
        // imageViewer.seadragonViewer.viewport.getBounds(true).getCenter();
        // viewportLocation = Seadragon.Utils.calcRotatedPoint(viewportLocation,
        // viewportCenter, negRotate);

        // console.log('natural height: ' +
        // imageViewer.seadragonViewer.source.height);
        // console.log('natural width: ' +
        // imageViewer.seadragonViewer.source.width);

        var height = imageViewer.seadragonViewer.source.dimensions.y
            / imageViewer.seadragonViewer.source.dimensions.x;

        // create a rectangle representing content size, rotate it, and find the
        // top and bottom of the content
        var contentRect = new Seadragon.Rect(0, 0, 1, height);
        var rotatedRect = Seadragon.Utils.calcRotatedRect(contentRect,
            contentRect.getCenter(), Seadragon.Config.rotation);
        var top = rotatedRect.getTopLeft().y;
        var bottom = rotatedRect.getBottomRight().y;

        // console.log('top: ' + top);
        // console.log('bottom: ' + bottom);
        // console.log('viewport point: ' + viewportLocation);
        // console.log('height: ' + height);
        // // console.log('rotated point: ' + rotatedPoint);
        // console.log('delta y: ' + delta.y);
        // console.log('\n\n');

        // if moving previous
        if (viewportLocation.y < top && delta.y > 0 && !inDrag) {
          galleryPanel.pullSwitchImage('up', getPreviousImageThumbnailUrl());
        }

        // if moving next
        else if (viewportLocation.y > bottom && delta.y < 0 && !inDrag) {
          galleryPanel.pullSwitchImage('dn', getNextImageThumbnailUrl());
        }

        else {
          origDragHandler(tracker, position, delta);
        }

        inDrag = true;
      }
    };

    // set the bounds properly on open
    imageViewer.seadragonViewer.addEventListener('open', function() {
      if (imageViewer.seadragonViewer.viewport) {
        if (imageSwitchType === 'click_zoom') {
          imageViewer.seadragonViewer.viewport.fitBounds(lastRecordedBounds,
              true);
        } else if (imageSwitchType === 'pull_up_zoom'
            || imageSwitchType === 'pull_dn_zoom') {

          // store variables for computation
          var height = imageViewer.seadragonViewer.source.dimensions.y
              / imageViewer.seadragonViewer.source.dimensions.x;
          var topLeft = lastRecordedBounds.getTopLeft();
          var bottomRight = lastRecordedBounds.getBottomRight();
          var pullBounds = null;

          // if pulling next
          if (imageSwitchType === 'pull_dn_zoom') {
            pullBounds = new Seadragon.Rect(topLeft.x,
                -(bottomRight.y - height), lastRecordedBounds.width,
                lastRecordedBounds.height);
          }

          // if we're pulling prev
          else {
            pullBounds = new Seadragon.Rect(topLeft.x, -1 * (topLeft.y)
                + height - lastRecordedBounds.height, lastRecordedBounds.width,
                lastRecordedBounds.height);
          }

          // apply the pull zoom
          // TODO: something is calling fitbounds after this point so we use to
          // make this one go last
          // still, this is a race condition we'd rather avoid
          setTimeout(function() {
            imageViewer.seadragonViewer.viewport.fitBounds(pullBounds, true);
          }, 100);
        }
      }
    });
     */
  };

  /**
   * Jumps to the image at the specified index (1 indexed not 0) Switch
   * maintains current zoom
   * 
   * @param index
   *            of the image to jump to
   */
  GalleryPanel.prototype.jumpToImage = function(index) {
    var row = $('#thumbnail_strip table').find('tr:nth-child(' + index + ')');
    var thumbnailUrl = row.find('img.thumbnail').attr('file');
    imageSwitchType = 'click_zoom';
    switchImage(this.imageViewer, thumbnailUrl, true);
  };

  /**
   * Switches the image displayed in the image viewer The zoom level is entirely
   * reset
   * 
   * @param thumbnailUrl
   *            url of the thumbnail to switch image for
   */
  GalleryPanel.prototype.switchImage = function(thumbnailUrl) {
    imageSwitchType = 'no_zoom';
    switchImage(this.imageViewer, thumbnailUrl);
  };

  /**
   * Switches the image displayed in the viewer using a click mentality This is
   * intended for use when a user clicks on an image in the gallery
   * 
   * @param thumbnailUrl
   *            of the image to switch to
   */
  GalleryPanel.prototype.clickSwitchImage = function(thumbnailUrl) {
    imageSwitchType = 'click_zoom';
    switchImage(this.imageViewer, thumbnailUrl);
  };

  /**
   * Switches the image displayed in the viewer using the pull mentality This is
   * intended for use when a user pulls to the next or previous image in the
   * gallery
   * 
   * @param direction
   *            of the pull ('up'|'dn')
   * @param thumbnailUrl
   *            of the image to pull to
   */
  GalleryPanel.prototype.pullSwitchImage = function(direction, thumbnailUrl) {
    if (direction === 'up' || direction === 'dn') {
      imageSwitchType = 'pull_' + direction + '_zoom';
      switchImage(this.imageViewer, thumbnailUrl, true);
    }
  };

  // return the gallery panel constructor
  return GalleryPanel;

}(window, jQuery));