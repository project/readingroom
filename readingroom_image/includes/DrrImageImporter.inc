<?php
/**
 * @file
 * file for class DrrImageImporter
 */

/**
 * importer class for images
 */
class DrrImageImporter extends DrrBaseImporter {

  protected static $WP_TYPE_SYS = 0;

  protected static $IMG_TYP_STD = 1;
  protected static $IMG_TYP_MFS = 2;
  protected static $IMG_TYP_UAD_DIR = 3;

  private $imported;  // images imported within the current directory
  private $waypoints;  // waypoints (within the current directory)

  /**
   * constructor
   */
  function __construct() {
    // nothing to do, just prevent to call the base constructor
  }

  /**
   * start the import process
   */
  public function start_import() {
    $this->import_dir_rec(self::$params['base dir']);
  }

  /**
   * start the import
   * @param IDrrCsvImporter $importer
   */
  protected function execute_csv_import(IDrrCsvImporter $importer) {
    $importer->start_import();
  }

  /**
   * deliver the corresponding CsvLoad for this class
   * @param $type
   * @param $dir
   * @param $file
   * @param $fileID
   * @return DrrDummyCsvLoader|DrrImageWaypointImporter
   */
  protected function get_csv_loader($type, $dir, $file, $fileID) {
    switch ($type) {
      case 'waypoint':
        return new DrrImageWaypointImporter(self::$params['doc root'] . $dir, $file);
      default:
        return new DrrDummyCsvLoader();
    }
  }

  /**
   * check if e fileneme fits to MFS file name format
   * @param string $file
   * @return boolean is true if the filename fits to MFS file name standard
   */
  private function hasMfsFormat($file) {
    if (preg_match('/^[0-9]{7,9}_[0-9]{5}[.]/', $file)) return TRUE;
    return FALSE;
  }

  /**
   * import files from the root and sub directories recursively
   * @param String $dir root directory
   */
  private function import_dir_rec($dir) {
    if (strlen($dir) > 0 && substr($dir, -1) != DIRECTORY_SEPARATOR) {
      $dir .= DIRECTORY_SEPARATOR;
    }
    $my_dir = self::$params['doc root'] . $dir;
    $file_list = scandir($my_dir);

    foreach ($file_list as $file) {
      if (substr($file, 0, 1) == '.') continue;
      if (is_dir($my_dir . '/' . $file)) {
        if (substr($file, -6) == '_files' && in_array(substr($file, 0, strlen($file) - 6) . '.xml', $file_list)) continue;  // skip DeepZoom folders
        $this->import_dir_rec($dir . $file);
      }
    }
    $this->import_dir_spec($dir, $file_list);
  }

  /**
   * import a specific directory
   * @param String $dir
   * @param array $file_list
   */
  private function import_dir_spec($dir, $file_list) {
    $this->waypoints = array();
    $this->imported = array();

    // load mormal image files
    foreach ($file_list as $file) {
      if (is_dir(self::$params['doc root'] . $dir . '/' . $file)) continue;  // skip each directory
      $suffix = strtolower(substr($file, strrpos($file, '.')));

      // check for DeepZoom
      if ($suffix == '.xml' && in_array(substr($file, 0, strlen($file) - 4) . '_files', $file_list)) {
        $this->import_image_file($dir, $file);
      }
      elseif ($suffix == '.tif' || $suffix == '.jpg') {
        $this->import_image_file($dir, $file);
      }
      elseif ($suffix == '.csv') {
        $this->import_csv_file($dir, $file, "image");
      }
    }
  }

  /**
   * import an unclassified image file
   * @param String $dir directory of the image
   * @param String $file image filename
   */
  private function import_image_file($dir, $file) {
    if (in_array($file, $this->imported)) return;

    // get waypoint type and key
    $image_type = self::$IMG_TYP_STD;
    $key = self::$params['doc root'] . $dir;

    if ($this->hasMfsFormat($file)) {
      $image_type = self::$IMG_TYP_MFS;
      $key = substr($file, 0, strpos($file, '_'));
    }
    else {
      if (self::$params['dir uad']) {
        $image_type = self::$IMG_TYP_UAD_DIR;
        $key = substr(strrchr(substr($dir, 0, strlen($dir) - 1), DIRECTORY_SEPARATOR), 1);;
      }
    }

    if (!array_key_exists($key, $this->waypoints)) {
      $loaded = $this->load_waypoint_by_image_type_and_key($image_type, $key);
      if (!$loaded) {
        // if no waypoint found create a new one
        $waypoint_entity = readingroom_entity_create(DRR_ENTITY_WAYPOINT, DRR_BUNDLE_WAYPOINT);
        /* @var $waypoint_entity EntityDrupalWrapper */
        $waypoint_entity->{DRR_PREFIX . 'type'} = self::$WP_TYPE_SYS; // system generated waypoint
        $waypoint_entity->{DRR_PREFIX . 'title'} = ($image_type == self::$IMG_TYP_STD ? $dir : $key);
        $waypoint_entity->{DRR_PREFIX . 'access_level'} = variable_get(DRR_PREFIX . 'i_def_wp', 1);
        $waypoint_entity->{DRR_PREFIX . 'import_ref'} = $this->get_import_id();
        if ($image_type == self::$IMG_TYP_MFS) {  // mfs
          $dgs = (int)$key;
          $waypoint_entity->{DRR_PREFIX . 'folder_id'} = $dgs;
        }
        else {
          $waypoint_entity->{DRR_PREFIX . 'path'} = $dir;

          switch ($image_type) {
            case self::$IMG_TYP_STD:
              $waypoint_entity->{DRR_PREFIX . 'md5'} = $key;
              break;

            case self::$IMG_TYP_UAD_DIR:
              $waypoint_entity->{DRR_PREFIX . 'uad'} = $key;
          }
        }
        $waypoint_entity->save();
        $this->waypoints[$key] = array(
          'waypoint' => $waypoint_entity,
          'max pos' => -1,  // maximum number of sort position used by images
        );
      }
      else { // handle waypoint found

        $wp = entity_metadata_wrapper(DRR_ENTITY_WAYPOINT, $loaded, array('bundle' => DRR_BUNDLE_WAYPOINT));
        /* @var $wp EntityDrupalWrapper */

        // load images for this waypoint
        $max_sort = 0;
        foreach (_readingroom_image_load_images_by_waypoint_id($wp->id->value()) as $image) {
          $this->imported[] = substr($image[DRR_PREFIX . 'url']['value'], strrpos($image[DRR_PREFIX . 'url']['value'], '/') + 1);
          if ($max_sort < $image[DRR_PREFIX . 'position']['value']) $max_sort = $image[DRR_PREFIX . 'position']['value'];
        }

        // save waypoints into cache
        $this->waypoints[$key] = array(
          'waypoint' => $wp,
          'max pos' => $max_sort,  // maximum number of sort position used by images
        );
      }
    }
    if (in_array($file, $this->imported)) return;

    // convert image
    if (self::$params['image convert']) {
      $file = DeepZoomConverter::convert_image($file, self::$params['doc root'] . $dir);
    }

    // create the image
    $image_entity = readingroom_entity_create(DRR_ENTITY_IMAGE, DRR_BUNDLE_IMAGE);
    /* @var $image_entity EntityDrupalWrapper */
    $image_entity->{DRR_PREFIX . 'import_ref'} = $this->get_import_id();
    $image_entity->{DRR_PREFIX . 'filename'} = $file;
    $image_entity->{DRR_PREFIX . 'path'} = $dir . (substr($dir, -1) == '/'?'':'/');
    $image_entity->{DRR_PREFIX . 'access_level'} = variable_get(DRR_PREFIX . 'i_def_im', 1);
    $image_entity->{DRR_PREFIX . 'def_waypoint_ref'} = $this->waypoints[$key]['waypoint']->id->value();
    if ($image_type == self::$IMG_TYP_MFS) {
      $image_entity->{DRR_PREFIX . 'folder_id'} = (int)$key;
      $image_entity->{DRR_PREFIX . 'image_name'} = (int)substr(substr($file, 0, strpos($file, '.')), strpos($file, '_') + 1);
    }
    if ($this->waypoints[$key]['waypoint']->{DRR_PREFIX . 'uad'}->value() != "") {
      $uad = $this->waypoints[$key]['waypoint']->{DRR_PREFIX . 'uad'}->value();
      $image_entity->{DRR_PREFIX . 'folder_uad'} = $uad;
      $pattern = variable_get(DRR_PREFIX . 'uad_pattern', '/(\w{3})(.*).(xml|jpg)/i');
      $replacement = variable_get(DRR_PREFIX . 'uad_replacement', '$2');
      $image_entity->{DRR_PREFIX . 'image_name'} = preg_replace($pattern, $replacement, str_replace($uad, "", $file));
    }
    $image_entity->save();

    // add image to waypoint
    $image_list = readingroom_entity_create(DRR_ENTITY_IMAGELIST, DRR_BUNDLE_IMAGELIST);
    /* @var $image_list EntityDrupalWrapper */
    $image_list->{DRR_PREFIX . 'waypoint_ref'} = $this->waypoints[$key]['waypoint']->id->value();
    $image_list->{DRR_PREFIX . 'image_ref'} = $image_entity->id->value();
    $image_list->{DRR_PREFIX . 'url'} = $image_entity->{DRR_PREFIX . 'path'}->value() . $image_entity->{DRR_PREFIX . 'filename'}->value();
    $image_list->{DRR_PREFIX . 'position'} = ++$this->waypoints[$key]['max pos'];
    $image_list->{DRR_PREFIX . 'is_blank'} = 0;
    $image_list->save();
  }

  /**
   * load a waypoint identified by a ID field and it's corresponding key
   * @param $id_field
   * @param $value
   * @return bool|mixed
   */
  protected function load_waypoint_by_field_id($id_field, $value) {
    $result = (new EntityFieldQuery())
      ->entityCondition('entity_type', DRR_ENTITY_WAYPOINT)
      ->fieldCondition($id_field, 'value', $value, '=')
      ->execute();
    if (empty($result)) return FALSE;

    $result = reset($result);
    $result = reset($result);
    $result = entity_load(DRR_ENTITY_WAYPOINT, array($result->id));
    return reset($result);
  }

  /**
   * load a waypoint identified by image type and corresponding key
   * @param $type int
   * @param $key mixed
   * @return mixed
   */
  protected function load_waypoint_by_image_type_and_key($type, $key) {
    switch ($type) {
      case self::$IMG_TYP_MFS:
        return $this->load_waypoint_by_field_id(DRR_PREFIX . 'folder_id', $key);
        break;

      case self::$IMG_TYP_UAD_DIR:
        return $this->load_waypoint_by_field_id(DRR_PREFIX . 'uad', $key);
        break;

      default:
        return $this->load_waypoint_by_field_id(DRR_PREFIX . 'md5', $key);
        break;
    }
  }

  /**
   * load a waypoint identified by it's ID (DGS | UAD)
   * @param $id int|string
   * @return bool|mixed
   */
  protected function waypointLoadByID($id) {
    return $this->load_waypoint_by_field_id(is_numeric($id) ? DRR_PREFIX . 'folder_id' : DRR_PREFIX . 'uad', $id);
  }

  /**
   * TODO: function not in use - just hold for future development (DCamX import)
   * @param $dir
   * @param $file
   */
  /*
  private function importFile_DCamX($dir, $file) {
    $md5 = md5_file($dir . '/' . $file);
    if ($this->has_file_imported($file, $dir, $md5)) return;

    // parse DCamX
    $xml = simplexml_load_file($dir . '/' . $file);
    $json = json_encode($xml);
    $options = json_decode($json, TRUE);

    // create waypoint
    $folder = $options['folder'];
    $waypoint = array(
      'type' => 0,
      'title' => $folder['record-title'],
      'import_id' => $this->get_import_id(),
      'path' => $dir,
      'locality' => $folder['locality'],
      'subject' => $folder['subject'],
      'dates' => $folder['record-dates'],
      'folder_id' => 0,
      'md5' => $md5,
    );
    $waypoint = $this->waypointCreate($waypoint);
    $entries = $options['entry'];
    for ($i = 0; $i < count($entries); $i++) {
      $entry = $entries[$i]['@attributes'];
      //      $test =
      if ($entry['blankpage'] == 'true') {
        $this->toImport[] = array(
          'filename' => '',
          'path' => '',
          'is_blank' => 1,
          'sort_position' => $i + 1,
          'waypoint_id' => $waypoint['id_waypoint'],
          'import_id' => $this->get_import_id(),
        );

      } else {
        $filename = substr($entry['filename'], strrpos($entry['filename'], '\\') + 1);
        $this->imported[] = $filename;
        $this->toImport[] = array(
          'filename' => $filename,
          'path' => $dir,
          'sort_position' => $i + 1,
          'waypoint_id' => $waypoint['id_waypoint'],
          'import_id' => $this->get_import_id(),
        );
      }
    }
  }
  */

  /**
   * TODO: function not in use - just hold for future development
   * desc
   * description
   * @param unknown_type $waypoint waypoint
   */
  /*
  private function waypointCreate($waypoint) {
    $controller = entity_get_controller('readingroom_waypoint');
    return $controller->save($waypoint);
  }
  */

  /**
   * TODO: function not in use - just hold for future development (DCam2 import)
   * @param $dir
   * @param $file
   */
  /*
  private function importFile_Log($dir, $file) {
    $md5 = md5_file($dir . '/' . $file);
    if ($this->has_file_imported($file, $dir, $md5)) return;

    // create the waypoint
    $help = explode('_', substr($file, 0, strpos($file, '.')));
    $waypoint = array(
      'type' => 0,
      'title' => $help[0],
      'import_id' => $this->get_import_id(),
      'path' => $dir,
      'subject' => $help[1],
      'folder_id' => 0,
      'md5' => $md5,
    );
    $waypoint = $this->waypointCreate($waypoint);

    // import the images
    $file_array = file($dir . '/' . $file);
    $sort = 0;  // sort position
    foreach ($file_array as $lineNumber => $line) {
      if ($lineNumber == 0) continue;
      $help = substr($line, 1, strpos($line, ',') - 2);

      $this->imported[] = $help;
      $this->toImport[] = array(
        'filename' => $help,
        'path' => $dir,
        'sort_position' => ++$sort,
        'waypoint_id' => $waypoint['id_waypoint'],
        'import_id' => $this->get_import_id(),
      );
    }
  }
  */
}
