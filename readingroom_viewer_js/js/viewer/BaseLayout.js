/**
 * @file
 * Image Gallery for the image viewer
 */
var BaseLayout = (function(window, $) {
  "use strict";

  /** The image viewer for the layout **/
  var layoutViewer = null;
  
  /** Images to use in the gallery **/
  var galleryImages = null;
  
  /** Selected index into the gallery images **/
  var selectedImageIndex = null;
  
  /** Overrides for the control panel buttons **/
  var buttonOverrides = null;

  /**
   * Constructs a new BaseLayout
   * @param imageViewer to layout
   */
  var BaseLayout = function(imageViewer) {
    layoutViewer = imageViewer;
      galleryImages = new Array();
      this.galleryPanel = null;
  };
  
  /**
   * Sets the gallery images to load
   * @param images to load
   * @param selectedIndex of the gallery images
   */
  BaseLayout.prototype.setGalleryImages = function(images, selectedIndex) {
      galleryImages = images;
      selectedImageIndex = selectedIndex?selectedIndex:0;
  };

  /**
   * Apply the configured layout to the image viewer 
   * None of the configuration takes place until this function is called
   * This function does the hard work of figuring out how everything should appear
   */
  BaseLayout.prototype.applyLayout = function() {
    if (galleryImages) {
      this.galleryPanel = new GalleryPanel($('body'), layoutViewer, galleryImages, selectedImageIndex);
    }
//      new ControlPanel(layoutViewer.viewerDiv, layoutViewer, buttonOverrides);  // new FS Contropanel - TJB
  };

  return BaseLayout;
  
})(window, jQuery);
