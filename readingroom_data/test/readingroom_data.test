<?php
/**
 * @file
 * provides tests for data structures of readingroom_data
 */


/**
 * Tests the functionality of the Simpletest example content type.
 */
class ReadingRoomDataTestCase extends DrupalWebTestCase {
//  protected $privileged_user;

  /*
   * provide Info for ReadingRoomDataTestCase
   */
  public static function getInfo() {
    // Note: getInfo() strings are not translated with t().
    return array(
      'name' => 'Base Data Test',
      'description' => 'Check if the creation of basic data structure for ReadingRoom has succeeded',
      'group' => 'ReadingRoom',
    );

  }

  /*
   * setup test environment
   */
  public function setUp() {
    // Enable any modules required for the test. This should be an array of module names.
    parent::setUp(array('readingroom_data'));
  }

  /*
   * public test function for data of readingroom_data
   */
  public function test_ReadingRoomData() {
    //verify that admin functionality cannot be accessed by an anonymous user
    $this->noAccessForAnonymousUsersTest();

    //login in as an authorized user
    $this->loginAsAuthorizedUser();

    //verify that authorized users can see entities have created during module enable
    $this->entitiesExistTest();
  }

  /*
   * check if entities are created
   */
  protected function entitiesExistTest() {
    foreach ($this->getEntityList() as $entity) {
      $this->drupalGet('admin/structure/entity-type/' . $entity['name']);
      $this->assertNoText(t('Access denied'), t('Entity structure is be available to non-admin users.'));
      $this->assertNoText(t('Add entity type'), t('The entity url hasn\'t be found.'));
      $this->assertText(t($entity['label']), t('Entity @label has been created.', array('@label' => $entity['label'])));
      foreach ($entity['bundles'] as $bundle) {
        $this->drupalGet('admin/structure/entity-type/' . $entity['name'] . '/' . $bundle['name']);
        $this->assertNoText(t('Access denied'), t('Bundle structure is be available to non-admin users.'));
        $this->assertNoText(t('Add bundle'), t('The bundle url hasn\'t be found.'));
        $this->assertText(t($bundle['label']), t('Bundle @label has been created.', array('@label' => $bundle['label'])));
        $this->drupalGet('admin/structure/entity-type/' . $entity['name'] . '/' . $bundle['name'] . '/fields');
        foreach ($bundle['fields'] as $field) {
          $this->assertText($field, t('Entity @entity has to have the field @field.', array(
            '@entity' => $entity['name'],
            '@field' => $field,
          )));
        }
      }
    }
  }

  /*
   * get a list of entities and bundles to be installed by readingroom_data
   */
  protected function getEntityList() {
    return array(
      array(
        'name' => DRR_ENTITY_IMPORT,  // machine name for the entity
        'label' => DRR_PREFIX_HUMAN . t('Imports'),   // human readable entity name
        'bundles' => array(
          array(
            'name' => DRR_BUNDLE_IMPORT,  // machine name for the bundle
            'label' => DRR_PREFIX_HUMAN . t('Import'),   // human readable bundle name
            'fields' => array(
              DRR_PREFIX . 'start_time',
              DRR_PREFIX . 'status',
              DRR_PREFIX . 'status_time',
            ),
          ),
        ),
      ),
    );
  }

  /**
   * Tests that anonymous users cannot access the administration pages
   */
  private function noAccessForAnonymousUsersTest() {

    //retrieve the readingroom structures page and ensure that access is denied (for non-admin user)
    $this->drupalGet('admin/structure/entity-type');
    $this->assertText(t('Access denied'), t('Entity structure is not be available to non-admin users.'));
  }

  /**
   * Logs in as an authorized user
   */
  private function loginAsAuthorizedUser() {
    $auth_user = $this->drupalCreateUser(
      array(
        'access administration pages',
        'eck administer entity types',
        'eck administer bundles',
        'eck administer entities',
      )
    );
    $this->drupalLogin($auth_user);
  }
}
