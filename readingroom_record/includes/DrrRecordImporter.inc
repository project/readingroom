<?php
/**
 * @file
 * file for class DrrRecordImporter
 */

/**
 * Class DrrRecordImporter
 */
class DrrRecordImporter extends DrrBaseImporter implements IDrrCsvImporter {
  private $path;

  /**
   * public constructor
   */
  function __construct() {
    // nothing to do, just prevent to call the base constructor
  }

  /**
   * start record import process
   */
  public function start_import() {
    include_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'readingroom') . '/includes/IDrrCsvImporter.inc';
    $this->import_dir_rec(self::$params['base dir']);
  }

  /**
   * import files from the root and sub directories recursively
   * @param String $dir root directory
   */
  private function import_dir_rec($dir) {
    $my_dir = self::$params['doc root'] . $dir;
    $fileList = scandir($my_dir);

    foreach ($fileList as $file) {
      if (substr($file, 0, 1)=='.') continue;
      if (is_dir($my_dir . '/' . $file)) {
        if (substr($file, -6)=='_files' && in_array(substr($file, 0, strlen($file)-6) . '.xml', $fileList)) continue;  // skip DeepZoom folders
        $this->import_dir_rec($dir . $file . DIRECTORY_SEPARATOR);
      }
    }
    $this->import_dir_spec($dir, $fileList);
  }

  /**
   *
   * Enter description here ...
   * @param String $dir
   * @param array $fileList list of files within the directory
   */
  private function import_dir_spec($dir, $fileList) {
    foreach ($fileList as $file) {
      if (is_dir(self::$params['doc root'] . $dir . $file)) continue;  // skip each directory
      $suffix = substr($file, strrpos($file, '.'));

      // check for record file
      if ($suffix == '.csv' || $suffix == '.txt') {
        $this->import_csv_file($dir, $file, "record");
      }
    }
  }

  /**
   * get the corresponding importer
   * @param $type
   * @param $dir
   * @param $file
   * @param $fileID
   * @return DrrDummyCsvLoader|DrrRecordDataImporter|null
   */
  protected function get_csv_loader($type, $dir, $file, $fileID) {
    switch ($type) {
      case 'data':
        return new DrrRecordDataImporter($dir, $file, $fileID);
      default:
        return new DrrDummyCsvLoader();
    }
  }

  /**
   * start the importer
   * @param IDrrCsvImporter $importer
   */
  protected function execute_csv_import(IDrrCsvImporter $importer) {
    $importer->start_import();
  }
}