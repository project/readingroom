<?php
/**
 * @file
 * file for class DrrRecordDataImporter
 */


/**
 * Class DrrRecordDataImporter
 */
class DrrRecordDataImporter extends DrrRecordImporter implements IDrrCsvImporter {
  private $path;
  private $fileID;  // id of the csv-file
  private $bundle = 'record';
  private $event_type_def = 'event';
  private $roles = array();
  private $analyze_lines = 100; // analyze 100 lines of a column to guess the data type
  private $separator = false;
  private $delimiter = '"';
  private $months = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
  private $ignore = array();
  private $actions = array();
  private $headers = array();
  private $locations = array();
  private $columns_assignd = array();  // columns defined in settings

  /**
   * public constructor
   * @param $dir
   * @param $file
   * @param $fileID
   */
  public function __construct($dir, $file, $fileID) {
    $this->path = $dir . $file;
    $this->fileID = $fileID;
  }

  /**
   * start the import
   * @throws Exception
   */
  public function start_import() {
    $fh = fopen(self::$params['doc root'] . $this->path, 'r');
    fgets($fh);  // skip first line
    $line = fgets($fh);

    // read all setup lines
    while ($line[0] == '#') {
      $this->analyzeSettings($line);
      $line = fgets($fh);
    }
    if (!$this->separator) $this->separator = _readingroom_get_separator($line);
    $this->reorganizePersons();
    $this->analyzeHeaders($line);
    $this->createBundles();

    $result = array();
    $this->readBulkCsvRows($result, $fh, $this->analyze_lines);
    $this->getDatatypesFromRows($result);
    $this->createBundleFields();

    self::watchdog('Start to import data');
    // import all lines used for bulk read
    foreach ($result as $line) {
      $this->import_line_array($line);
    }
    $result = NULL; // free memory

    // import all other lines
    $i = $this->analyze_lines;
    gc_enable();
    while (($line=fgetcsv($fh, 0, $this->separator, $this->delimiter))!==FALSE) {
      $i++;
      if (($i % 2000) == 0) {
        $this->watchdog('importing line number @number', array('@number' => $i));
      }
      try {
        $this->import_line_array($line);
      } catch (Exception $e) {
        self::watchdog('Exception line @number: @error', array('@error' => $e->getMessage(), '@number' => $i), WATCHDOG_WARNING);
      }
    }
    fclose($fh);
  }

  /**
   * import a data line
   * @param $line
   * @throws Exception
   */
  private function import_line_array($line) {
    $record_entity = readingroom_entity_create(DRR_ENTITY_RECORD, $this->getEntityBundleName(DRR_ENTITY_RECORD));
    $record_entity->{DRR_PREFIX . 'import_file_ref'} = $this->fileID;
    foreach ($this->actions[DRR_ENTITY_RECORD] as $action) {
      $field_name = DRR_PREFIX . $action['field'];
      if ($action['field']=='location') {
        $location_name = $line[$action['column_id']];
        if (!isset($this->locations[$location_name])) {
          $this->locations[$location_name] = $this->getTaxonomyTermId($line[$action['column_id']], DRR_TAXONOMY_LOCATION); //$location->tid;
        }
        $value = $this->locations[$location_name];
      }
      else {
        $value = $line[$action['col ID']];
        if ($action['type']=='number_integer' && $value=="") $value = 0;
        if ($action['field']=='month') $value = $this->getMonthNumber($value);
      }
      try {
        $record_entity->$field_name = $value;
      } catch (Exception $e) {
        throw new Exception(t('@name: Value not compatible to column.', array('@name' => $action['field'])));
      }
    }
    try {
      if (module_exists('readingroom_image')) {
        $query = new EntityFieldQuery();
        $result = $query->entityCondition('entity_type', DRR_ENTITY_IMAGE)
          ->fieldCondition(DRR_PREFIX . 'folder_id', 'value', $record_entity->{DRR_PREFIX . 'folder_id'}->value())
          ->fieldCondition(DRR_PREFIX . 'image_name', 'value', (int) $record_entity->{DRR_PREFIX . 'image_number'}->value())
          ->execute();
        if ($result) {
          $result = reset($result);
          $result = reset($result);
          $record_entity->{DRR_PREFIX . 'image_ref'} = $result->id;
        }
      }
    }
    catch (Exception $e) {
      // do nothing, just preventing error
    }
    $record_entity->save();

    // write person entities
    $dataPersons = $this->actions[DRR_ENTITY_PERSON];
    foreach ($dataPersons as $roleName => &$rolePerson) {
      $person_entity = readingroom_entity_create(DRR_ENTITY_PERSON, $this->getEntityBundleName(DRR_ENTITY_PERSON));

      if (!isset($this->roles[$roleName])) {
        $this->roles[$roleName] = $this->getTaxonomyTermId($roleName, DRR_TAXONOMY_PERSON_ROLE); //$role->tid;
      }
      $person_entity->{DRR_PREFIX . 'person_role'} = $this->roles[$roleName];

      foreach ($rolePerson as $value) {
        $person_entity->{DRR_PREFIX . $value['field']} = trim($this->decode_utf8($line[$value['col ID']]));
      }
      // save persons having at least given or surname
      if ($person_entity->{DRR_PREFIX . 'surname'}->value() || $person_entity->{DRR_PREFIX . 'givenname'}->value()) {
        $person_entity->save();
        $rolePerson['id'] = $person_entity->id->value();
      }
    }

    // construct the events
    $dataEvents = array();
    foreach ($this->actions[DRR_ENTITY_EVENT] as $eventName => $event) {
      foreach ($event['fields'] as $field) {
        if (strlen($line[$field['col ID']]) == 0) continue;
        if ($field['col ID'] == '...') continue;
        if ($line[$field['col ID']]) $dataEvents[$eventName][$field['field']] = $line[$field['col ID']];
      }
      if (isset($dataEvents[$eventName]) && isset($event['persons'])) $dataEvents[$eventName]['persons'] = $event['persons'];
    }
    foreach ($dataEvents as &$event) {
      if (isset($event['event_month']) && !is_numeric($event['event_month']))
        $event['event_month'] = $this->getMonthNumber(substr($event['event_month'], 0, 3));
      if (isset($event['event_date'])) {
        $parse_date = $this->parse_date($event['event_date']);
        if (!isset($event['event_day'])) $event['event_day'] = $parse_date['event_day'];
        if (!isset($event['event_month'])) $event['event_month'] = $parse_date['event_month'];
        if (!isset($event['event_year'])) $event['event_year'] = $parse_date['event_year'];
      }
      if (!isset($event['event_day'])) $event['event_day']=0;
      if (!isset($event['event_month'])) $event['event_month']=0;
      if (!isset($event['event_year'])) $event['event_year']=0;
      if (!isset($event['event_date'])) {
        $event['event_date'] = ($event['event_day']==0?'?':$event['event_day']) .
          ' ' . ($event['event_month']==0?'???':$event['event_month']);
        $event['event_date'] .= ' ' . ($event['event_year']==0?'????':$event['event_year']);
      }
    }

    // write event entities
    foreach ($dataEvents as $eventName => $eventData) {
      // write event data
      $event_entity = readingroom_entity_create(DRR_ENTITY_EVENT, $this->getEntityBundleName(DRR_ENTITY_EVENT));
      foreach ($eventData as $key => $value) {
        switch ($key) {
          case 'location':
            if (!isset($this->locations[$value])) {
              $this->locations[$value] = $this->getTaxonomyTermId($this->decode_utf8($value), DRR_TAXONOMY_LOCATION);
            }
            $event_entity->{DRR_PREFIX . 'location'} = $this->locations[$value];
            break;

          case 'event_date':
          case 'event_month':
          case 'event_day':
          case 'event_year':
            $event_entity->{DRR_PREFIX . $key} = $value;
            break;

          default:
            $event_entity->$key = $value;
            break;
        }
      }
      $event_entity->save();

      // connect the entities
      foreach ($dataPersons as $persons_role => $person) {
        if (!isset($person['id'])) continue; // ignore persons without data
        if (isset($event['persons']) && !in_array($persons_role, $event['persons'])) continue;
        $entry = readingroom_entity_create(DRR_ENTITY_ENTRY, $this->getEntityBundleName(DRR_ENTITY_ENTRY));
        $entry->{DRR_PREFIX . 'person_ref'} = $person['id'];
        $entry->{DRR_PREFIX . 'record_ref'} = $record_entity->id->value();
        $entry->{DRR_PREFIX . 'event_ref'} = $event_entity->id->value();
        $entry->{DRR_PREFIX . 'access_level'} = variable_get(DRR_PREFIX . 'r_def_entry', 1);
        $entry->save();
      }
    }
  }

  /**
   * sparate date fields
   * @param $date
   * @return array
   */
  private function parse_date($date) {
    $dateInfo = array(
      'year' => 0,
      'month' => 0,
      'day' => 0,
    );
    $dateArray = preg_split('/[\ -.]/', $date);
    foreach ($dateArray as $field) {
      if (strlen($field)==3) {
        $dateInfo['month'] = $this->getMonthNumber($field);
      }
      else {
        if (is_numeric($field)) {
          $field = intval($field);
          if (!$dateInfo['day'] && $field<32) {
            $dateInfo['day'] = $field;
          }
          elseif (!$dateInfo['month'] && $field<13) {
            $dateInfo['month'] = $field;
          }
          else {
            $dateInfo['year'] = $field;
          }
        }
      }
    }
    return $dateInfo;
  }

  /**
   * get number of a month
   * @param $monthName
   * @return int
   */
  private function getMonthNumber($monthName) {
    if (!array_key_exists(strtolower($monthName), $this->months)) return 0;
    return $this->months[strtolower($monthName)]+1;
  }

  /**
   * decode utf8
   * @param $string
   * @return string
   */
  private function decode_utf8($string) {
    return utf8_decode($string);
//    return mb_dec($string, 'UTF-8', TRUE);
  }

  /**
   * get taxonomy for a specific term
   * @param $term
   * @param $taxonomy
   * @return mixed
   */
  private function getTaxonomyTermId($term, $taxonomy) {
    //try to load term from database
    $taxonomy_term = taxonomy_get_term_by_name($term, $taxonomy);
    if (!$taxonomy_term) {
      // create a new term
      $vocab = taxonomy_vocabulary_machine_name_load($taxonomy);
      taxonomy_term_save((object) array(
        'name' => $term,
        'vid' => $vocab->vid,
      ));
      $taxonomy_term = taxonomy_get_term_by_name($term, $taxonomy);
    }
    $taxonomy_term = reset($taxonomy_term);
    return $taxonomy_term->tid;
  }

  /**
   * analyze headers to organize them into action groups
   * @param $headerLine
   */
  private function analyzeHeaders($headerLine) {
    // sort arrays and remove used headers
    $this->headers = array_flip(explode($this->separator , trim($headerLine)));
    $this->addColumnID($this->actions[DRR_ENTITY_RECORD]);
    foreach ($this->actions[DRR_ENTITY_EVENT] as &$event) {
      $this->addColumnID($event['fields']);
    }
    foreach ($this->actions[DRR_ENTITY_PERSON] as &$person) {
      $this->addColumnID($person);
    }

    // remove headers to be ignored
    foreach ($this->ignore as $ignore) {
      foreach ($this->headers as $key => $info) {
        if ($ignore == substr($key, 0, strlen($ignore))) {
          unset($this->headers[$key]);
        }
      }
    }
  }

  /**
   * analyze a line for settings
   * @param $line
   */
  private function analyzeSettings($line) {
    $setting = json_decode(substr($line, 1));
    if ($setting) {
      if (array_key_exists('bundle', $setting)) $this->bundle = $setting->bundle;
      if (array_key_exists('event_type_def', $setting)) $this->event_type_def = $setting->event_type_def;
      if (array_key_exists('analyze', $setting)) $this->analyze_lines = intval($setting->analyze);
      if (array_key_exists('separator', $setting)) $this->separator = $setting->separator;
      if (array_key_exists('months', $setting)) {
        foreach ($setting->months as &$month) {
          $month = strtolower($month);
        }
        $this->months = array_flip($setting->months);
      }
      if (array_key_exists('ignore', $setting)) {
        foreach ($setting->ignore as $value) $this->ignore[] = $value;
      }
      if (array_key_exists('fields', $setting)) {
        foreach ($setting->fields as $field => $header) {
          $value = $this->getPredefinedField($field);
          if ($value) {
            $this->columns_assignd[] = $header;
            if ($value['entity'] == DRR_ENTITY_EVENT) {
              $this->actions[DRR_ENTITY_EVENT][$this->event_type_def]['fields'][$header] = $value;
            }
            else {
              $this->actions[$value['entity']][$header] = $value;
            }
          }
          else {
            self::watchdog("Undefined field @field used.", array('@field' => $field));
          }
        }
      }
      if (array_key_exists('event', $setting)) {
        $this->actions[DRR_ENTITY_EVENT][$setting->event->type]['persons'][] = $setting->event->person;
        foreach ($setting->event->fields as $field => $header) {
          $value = $this->getPredefinedField($field);
          if ($value) {
            $this->columns_assignd[] = $header;
            $this->actions[$value['entity']][$setting->event->type]['fields'][$header] = $value;
          }
          else {
            self::watchdog("Undefined field @field used.", array('@field' => $field));
          }
        }
      }
    }
  }

  /**
   * get standard bundle description
   * @param $human_bundle_name
   * @return null|string
   */
  private function getBundleDescription($human_bundle_name) {
    return  t('@a bundel created during import.', array('@a' => DRR_PREFIX_HUMAN . t($human_bundle_name)));
  }

  /**
   * estimate the data types for record columns
   * @param $rows
   */
  private function getDatatypesFromRows($rows) {
    $rowCount = count($rows);
    foreach ($this->headers as $header => $colID) {
      $this->actions[DRR_ENTITY_RECORD][$header] = array(
        'col ID' => $colID,
        'entity' => DRR_ENTITY_RECORD,
        'field' => 't_' . strtolower($header),
      );
      $i=0;
      $data_found = FALSE;  // flag if some data has been found
      while ($i<$rowCount) {
        if (preg_match('/^\d*$/', $rows[$i][$colID]) != 1) {
          $this->actions[DRR_ENTITY_RECORD][$header]['type']='text';
          break;
        }
        if (strlen($rows[$i][$colID])>0) $data_found = TRUE;
        $i++;
      }
      if (!isset($this->actions[DRR_ENTITY_RECORD][$header]['type'])) {
        if (!$data_found) $this->actions[DRR_ENTITY_RECORD][$header]['type']='text';  // assume text format if no data has been found
        else {
          $this->actions[DRR_ENTITY_RECORD][$header]['type'] = 'number_integer';  // all found data has integer format
          $this->actions[DRR_ENTITY_RECORD][$header]['field'] = 'n_' . strtolower($header);
        }
      }
    }
  }

  /**
   * get standard bundle name
   * @param $entity_name
   * @return string
   */
  private function getEntityBundleName($entity_name) {
    return "{$entity_name}_{$this->bundle}";
  }

  /**
   * get predefined fields
   * @param $field_name
   * @return array|null
   */
  private static function getPredefinedField($field_name) {
    switch ($field_name) {
      case "dgs_nbr":
        return array('entity' => DRR_ENTITY_RECORD, 'field' => 'folder_id', 'type' => 'number_integer');
      case "image_nbr":
        return array('entity' => DRR_ENTITY_RECORD, 'field' => 'image_number', 'type' => 'number_integer');
      case "place":
        return array('entity' => DRR_ENTITY_EVENT, 'field' => 'location', 'type' => 'text');
      case "event_date":
        return array('entity' => DRR_ENTITY_EVENT, 'field' => 'event_date', 'type' => 'date');
      case "event_day":
        return array('entity' => DRR_ENTITY_EVENT, 'field' => 'event_day', 'type' => 'number_integer');
      case "event_month":
        return array('entity' => DRR_ENTITY_EVENT, 'field' => 'event_month', 'type' => 'number_integer');
      case "event_year":
        return array('entity' => DRR_ENTITY_EVENT, 'field' => 'event_year', 'type' => 'number_integer');
      case "pr_givenname":
        return array('entity' => DRR_ENTITY_PERSON, 'field' => 'givenname', 'role' => 'primary', 'type' => 'text');
      case "pr_surname":
        return array('entity' => DRR_ENTITY_PERSON, 'field' => 'surname', 'role' => 'primary', 'type' => 'text');
      case "pr_f_givenname":
        return array('entity' => DRR_ENTITY_PERSON, 'field' => 'givenname', 'role' => 'father', 'type' => 'text');
      case "pr_f_surname":
        return array('entity' => DRR_ENTITY_PERSON, 'field' => 'surname', 'role' => 'father', 'type' => 'text');
      case "pr_m_givenname":
        return array('entity' => DRR_ENTITY_PERSON, 'field' => 'givenname', 'role' => 'mother', 'type' => 'text');
      case "pr_m_surname":
        return array('entity' => DRR_ENTITY_PERSON, 'field' => 'surname', 'role' => 'mother', 'type' => 'text');
      case "sp_givenname":
        return array('entity' => DRR_ENTITY_PERSON, 'field' => 'givenname', 'role' => 'spouse', 'type' => 'text');
      case "sp_surname":
        return array('entity' => DRR_ENTITY_PERSON, 'field' => 'surname', 'role' => 'spouse', 'type' => 'text');
      case "sp_f_givenname":
        return array('entity' => DRR_ENTITY_PERSON, 'field' => 'givenname', 'role' => 'spouse father', 'type' => 'text');
      case "sp_f_surname":
        return array('entity' => DRR_ENTITY_PERSON, 'field' => 'surname', 'role' => 'spouse father', 'type' => 'text');
      case "sp_m_givenname":
        return array('entity' => DRR_ENTITY_PERSON, 'field' => 'givenname', 'role' => 'spouse mother', 'type' => 'text');
      case "sp_m_surname":
        return array('entity' => DRR_ENTITY_PERSON, 'field' => 'surname', 'role' => 'spouse mother', 'type' => 'text');

      default:
        return NULL;
    }
  }

  /**
   * create a standard bundle
   * @param $entity_name
   * @param $human_bundle_name
   */
  private function createBundle($entity_name, $human_bundle_name) {
    // try to load existing bundle
    $machine_name = $this->getEntityBundleName($entity_name, $this->bundle);
    $bundle = Bundle::loadByMachineName($machine_name);
    if (is_array($bundle)) $bundle = new Bundle();
    if (is_null($bundle)) $bundle =  new Bundle();
    if ($bundle->isNew) {
      $bundle->name = $machine_name;
      $bundle->label = $machine_name;
      $bundle->description = $this->getBundleDescription($human_bundle_name);
      $bundle->entity_type = $entity_name;
      $bundle->save();
    }

    // create default fields
    if ($entity_name == DRR_ENTITY_RECORD) {
      readingroom_field_create(array(
        'field_name' => DRR_PREFIX . 'import_file_ref',
        'entity_type' => DRR_ENTITY_RECORD,
        'bundle' => $machine_name,
        'description' => 'The corresponding file this record was imported from.',
        'type' => 'entityreference',
        'label' => 'DRR ' . t('Import File'),
        'settings' => array(
          'target_type' => DRR_ENTITY_IMPORT_FILES,
          'handler' => 'base',
          'handler_settings' => array(
            'target_bundles' => array($this->getEntityBundleName(DRR_ENTITY_IMPORT_FILES, $this->bundle))
          ),
        ),
        'widget' => array('type' => 'entityreference_autocomplete'),
      ));
      readingroom_field_create(array(
        'field_name' => DRR_PREFIX . 'image_ref',
        'entity_type' => DRR_ENTITY_RECORD,
        'bundle' => $machine_name,
        'description' => 'The image this record belongs to.',
        'type' => 'entityreference',
        'label' => 'DRR ' . t('Image'),
        'settings' => array(
          'target_type' => DRR_ENTITY_IMAGE,
          'handler' => 'base',
          'handler_settings' => array(
            'target_bundles' => array($this->getEntityBundleName(DRR_ENTITY_IMAGE, $this->bundle))
          ),
        ),
        'widget' => array('type' => 'entityreference_autocomplete'),
      ));
    }
  }

  /**
   * create the standard bundles
   */
  private function createBundles() {
    $this->createBundle(DRR_ENTITY_RECORD, t('Record'));
    $this->createBundle(DRR_ENTITY_PERSON, t('Person'));
    $this->createBundle(DRR_ENTITY_EVENT, t('Event'));
    $this->createEntryBundle();
  }

  /**
   * create the entry bundle
   */
  private function createEntryBundle() {
    $machine_name = $this->getEntityBundleName(DRR_ENTITY_ENTRY, $this->bundle);
    $bundle = Bundle::loadByMachineName($this->getEntityBundleName(DRR_ENTITY_ENTRY, $this->bundle));
    if (is_array($bundle)) $bundle = new Bundle();
    if (is_null($bundle)) $bundle =  new Bundle();
    if (!$bundle->isNew) return;

    $bundle->name = $machine_name;
    $bundle->label = $machine_name;
    $bundle->description = $this->getBundleDescription('Entry');
    $bundle->entity_type = DRR_ENTITY_ENTRY;
    $bundle->save();

    // create default fields
    $fields = array(
      'person' => array(
        'target' => DRR_ENTITY_PERSON,
        'human' => t('Person'),
      ),
      'event' => array(
        'target' => DRR_ENTITY_EVENT,
        'human' => t('Event'),
      ),
      'record' => array(
        'target' => DRR_ENTITY_RECORD,
        'human' => t('Record'),
      ),
    );

    foreach ($fields as $key => $field_info) {
      readingroom_field_create(array(
        'field_name' => DRR_PREFIX . $key . '_ref',
        'entity_type' => DRR_ENTITY_ENTRY,
        'bundle' => $machine_name,
        'description' => t('The @human for this entry.', array('@human' => $field_info['human'])),
        'type' => 'entityreference',
        'label' => DRR_PREFIX_HUMAN . t($field_info['human']),
        'settings' => array(
          'target_type' => $field_info['target'],
          'handler' => 'base',
          'handler_settings' => array(
            'target_bundles' => $this->getEntityBundleName($field_info['target'], $this->bundle)
          ),
        ),
      ));
    }

    // add access level field
    $entity = DRR_ENTITY_ENTRY;
    $field['entity_type'] = $entity;
    $field['bundle'] = $machine_name;
    $field['type'] = 'number_integer';
    $field['field_name'] = DRR_PREFIX . 'access_level';
    $field['label'] = DRR_PREFIX_HUMAN . t('Access Level');
    readingroom_field_create($field);
  }

  /**
   * create the fields for standard bundles
   */
  private function createBundleFields() {
    foreach ($this->actions[DRR_ENTITY_RECORD] as $field) {
      $this->createBundleField($field);
    }
    foreach ($this->actions[DRR_ENTITY_EVENT] as $event) {
      foreach ($event['fields'] as $field) {
        $this->createBundleField($field);
      }
    }
    foreach ($this->actions[DRR_ENTITY_PERSON] as $person) {
      foreach ($person as $field) {
        $this->createBundleField($field);
      }
    }
  }

  /**
   * create a field of a bundle
   * @param $fieldInfo
   */
  private function createBundleField(&$fieldInfo) {
    $fieldName = DRR_PREFIX . (array_key_exists('field', $fieldInfo)?
        $fieldInfo['field']:
        substr($fieldInfo['type'], 0, 1) . "_{$fieldInfo['field']}");

    //create default fields
    switch ($fieldInfo['entity']) {
      case DRR_ENTITY_PERSON:
        $field = array(
          'bundle' => $this->getEntityBundleName(DRR_ENTITY_PERSON),
          'entity_type' => DRR_ENTITY_PERSON,
          'type' => 'text',
          'field_name' => DRR_PREFIX . 'surname',
          'label' => DRR_PREFIX_HUMAN . t('Surname'),
        );
        readingroom_field_create($field);

        $field['field_name'] = DRR_PREFIX . 'givenname';
        $field['label'] = DRR_PREFIX_HUMAN . t('Givenname');
        readingroom_field_create($field);

        $role_field = array(
          'field_name' => DRR_PREFIX . 'person_role',
          'entity_type' => DRR_ENTITY_PERSON,
          'bundle' => $this->getEntityBundleName(DRR_ENTITY_PERSON),
          'label' => t('Role'),
          'description' => t('The role of a person within a record.'),
          'type' => 'taxonomy_term_reference',
          'module' => 'taxonomy',
          'settings' => array(
            'allowed_values' => array(
              0 => array(
                'vocabulary' => DRR_TAXONOMY_PERSON_ROLE,
                'parent' => 0,
              ),
            ),
          ),
          'display' => array(
            'default' => array(
              'label' => 'above',
              'module' => 'taxonomy',
              'settings' => array(),
              'type' => 'taxonomy_term_reference_link',
              'weight' => 2,
            ),
            'teaser' => array(
              'label' => 'above',
              'settings' => array(),
              'type' => 'hidden',
              'weight' => 0,
            ),
          ),
        );
        readingroom_field_create($role_field);
        break;

      case DRR_ENTITY_EVENT:
        $field['bundle'] = $this->getEntityBundleName(DRR_ENTITY_EVENT);
        $field['entity_type'] = DRR_ENTITY_EVENT;
        if (array_keys(['event_date', 'event_day', 'event_month', 'event_year'], $fieldInfo['field'])) {
          $field['type'] = 'number_integer';
          $field['field_name'] = DRR_PREFIX . 'event_day';
          $field['label'] = DRR_PREFIX_HUMAN . t('Day');
          readingroom_field_create($field);

          $field['field_name'] = DRR_PREFIX . 'event_month';
          $field['label'] = DRR_PREFIX_HUMAN . t('Month');
          readingroom_field_create($field);

          $field['field_name'] = DRR_PREFIX . 'event_year';
          $field['label'] = DRR_PREFIX_HUMAN . t('Year');
          readingroom_field_create($field);

          $field['type'] = 'text';
          $field['field_name'] = DRR_PREFIX . 'event_date';
          $field['label'] = DRR_PREFIX_HUMAN . t('Date');
          readingroom_field_create($field);
          return;
        }
        if ($fieldInfo['field'] == 'location') {
          $field['field_name'] = $fieldName;
          $field['type'] = 'taxonomy_term_reference';
          $field['module'] = 'taxonomy';
          $field['settings'] = array(
            'allowed_values' => array(
              0 => array(
                'vocabulary' => DRR_PREFIX . 'location',
                'parent' => 0,
              ),
            ),
          );
          $field['display'] = array(
            'default' => array(
              'label' => 'above',
              'module' => 'taxonomy',
              'settings' => array(),
              'type' => 'taxonomy_term_reference_link',
              'weight' => 2,
            ),
            'teaser' => array(
              'label' => 'above',
              'settings' => array(),
              'type' => 'hidden',
              'weight' => 0,
            ),
          );
          readingroom_field_create($field);
          return;
        }
        break;

      case DRR_ENTITY_RECORD:
        if (array_keys(['folder_id', 'image_number'], $fieldInfo['field'])) {
          if (module_exists('readingroom_image')) {
            readingroom_field_create(array(
              'field_name' => DRR_PREFIX . 'image_ref',
              'entity_type' => DRR_ENTITY_RECORD,
              'bundle' => $this->getEntityBundleName(DRR_ENTITY_RECORD),
              'description' => 'The corresponding image this record is connected to.',
              'type' => 'entityreference',
              'label' => 'DRR ' . t('Image'),
              'settings' => array(
                'target_type' => DRR_ENTITY_IMAGE,
                'handler' => 'base',
                'handler_settings' => array(
                  'target_bundles' => array($this->getEntityBundleName(DRR_ENTITY_IMAGE, DRR_BUNDLE_IMAGE))
                ),
              ),
              'widget' => array('type' => 'entityreference_autocomplete'),
            ));
          }
        }
        break;
    }

    $field['field_name'] = $fieldName;
    $field['entity_type'] = $fieldInfo['entity'];
    $field['bundle'] = $this->getEntityBundleName($fieldInfo['entity']);
    $field['type'] = $fieldInfo['type'];
    $field['label'] = $fieldInfo['field']{1}=='_'?substr($fieldInfo['field'], 2):$fieldInfo['field'];

    if ($fieldInfo['field']=='location' && $fieldInfo['entity'] != DRR_ENTITY_PERSON) {
      $field['type'] = 'taxonomy_term_reference';
      $field['module'] = 'taxonomy';
      $field['settings']= array(
        'allowed_values' => array(
          0 => array(
            'vocabulary' => DRR_PREFIX . 'location',
            'parent' => 0,
          ),
        ),
      );
      $field['display'] = array(
        'default' => array(
          'label' => 'above',
          'module' => 'taxonomy',
          'settings' => array(),
          'type' => 'taxonomy_term_reference_link',
          'weight' => 2,
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      );
    }
    readingroom_field_create($field);
  }

  /**
   * readl a bulk of lines
   * @param array $result save the line into thisa structure
   * @param $fileHandler file to read from
   * @param $maxRows number of rows to read
   */
  private function readBulkCsvRows(&$result, $fileHandler, $maxRows) {
    // read some lines to estimate data types
    $result = array();
    $currentRow = 0;
    while (($result[$currentRow]=fgetcsv($fileHandler, 0, $this->separator, $this->delimiter))!==FALSE && $currentRow++<$maxRows) {
    }
    if ($currentRow<$maxRows && $result[$currentRow]==FALSE) unset($result[$currentRow]);
  }


  /**
   * add the column id to a field array
   * @param $array
   */
  private function addColumnID(&$array) {
    foreach ($array as $header => $value) {
      $array[$header]['col ID'] = $this->headers[$header];
      unset($this->headers[$header]);
    }

  }

  /**
   * reorganize person data structure to be accessible by role
   */
  private function reorganizePersons() {
    $reorg = array();
    foreach ($this->actions[DRR_ENTITY_PERSON] as $header => $value) {
      $reorg[$value['role']][$header] = $value;
    }
    $this->actions[DRR_ENTITY_PERSON]=$reorg;
  }
}
