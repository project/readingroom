if (!window.Seadragon) {
  window.Seadragon = {};
}
var Seadragon = window.Seadragon;

(function() {
  var scripts = document.getElementsByTagName('script');
  var path= scripts[scripts.length-1].src.split('?')[0];
  var mydir= path.split('/').slice(0, -1).join('/')+'/';
  
  var html = [];
  var JS_SCRIPTS = [
    "js/openseadragon-bin-1.0.0/openseadragon.js",
    "js/viewer/ImageViewer.js",
    "js/viewer/ImageViewerUtils.js",
    "js/viewer/GalleryPanel.js"
  ];
  for (var i = 0; i < JS_SCRIPTS.length; i++) {
    html.push('<script type="text/javascript" src="');
    html.push(mydir);
    html.push(JS_SCRIPTS[i]);
    html.push('"></script>\n');
  }

    var CSS_SCRIPTS = [
      "css/viewer/ImageViewer.css"
    ];
    for (var i = 0; i < CSS_SCRIPTS.length; i++) {
      html.push('<link rel="stylesheet" type="text/css" href="');
      html.push(mydir);
      html.push(CSS_SCRIPTS[i]);
      html.push('" >\n');
  }
    
  document.write(html.join(''));
})();
